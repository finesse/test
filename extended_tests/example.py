import pykat # dont include this in the requirements!!!
import numpy as np

reference = np.array([[  9.913489402110650e-01,   7.350871523992930e-02,
                         1.168791602932690e-01,   9.345349847565670e+01,
                         4.190476681657210e-02,   1.068669151602970e+02,
                         4.133478818744360e-02,  -9.329795893423830e+01],
                      [  9.913462423266070e-01,   7.361502480212340e-02,
                         1.168791859991820e-01,   9.345343194528940e+01,
                         4.189252248722950e-02,   1.068776135077970e+02,
                         4.132203558260200e-02,  -9.330608276610690e+01],
                      [  9.913462423266070e-01,   7.361502480212340e-02,
                         1.168791859991820e-01,   9.345343194528940e+01,
                         4.189252248722950e-02,   1.068776135077970e+02,
                         4.132203558260200e-02,  -9.330608276610690e+01],
                      [  9.913462762011960e-01,   7.361502480212340e-02,
                         1.168790644718610e-01,   9.345343194528940e+01,
                         4.190147024907890e-02,   1.066821556938460e+02,
                         4.133135118672500e-02,  -9.330608276610690e+01]])
    
results = []

for i in [1,2,3,4]:
    base = pykat.finesse.kat()
    base.parse("""
    l l1 1 0 n1
    s s1 1 n1 n2
    m m1 1 0 0 n2 dump

    gauss g1 m1 n2 0.02 0

    ad a00 0 0 0 n2
    ad a10 1 0 0 n2
    ad a20 2 0 0 n2
    ad a02 0 2 0 n2

    yaxis lin abs:deg
    noxaxis

    map m1 astig.map
    map m1 tilt.map

    maxtem 2
    phase 0

    conf m1 knm_order 1234
    conf m1 integration_method %i
    """ % i)
    
    out = base.run()
    
    results.append(out.y.flatten())
    
results =  np.array(results)



comparison = np.isclose(results, reference, atol=1e-19)

if not comparison.all():
    print("RESULTS")
    print(repr(results))
    print("COMPARISON")
    print(comparison)
    assert(comparison.all())
else:
    print("RESULTS")
    print(repr(results))