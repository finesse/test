#-----------------------------------------------------------------------

maxtem 6

# modulation for cavity lock
const fMI 6264182.0 # on 125stFSR + modsep

# input laser, here waist of IMC
l i1 6 0 0 nin
gauss g1 i1 nin 5e-3 -42.836m
s sin 0 nin nIMC1in

######################## IB #####################

bs IMC1i 0 1 0 -45.0 nIMC1in dump nIMC1i1 dump
s  sIMC1 0.03 1.44963 nIMC1i1 nIMC1I2
bs IMC1o 0 1 0 -29.195 nIMC1I2 dump nIMC1o1 dump

# modulator for simplicity after IMC
s s0 1m nIMC1o1 nEO1
mod eom1 $fMI .1 1 pm 0 nEO1 nEO2

# distance from OptoCad 
s sM4 0.161 1 nEO2 nM4i

# gauss g2 derived from g1, just to force mode mismatch 
# to be computed on M4
gauss g2 sM4 nM4i 5.0000014m 132.7421m 5.000009m 139.85894m

bs M4 1 0 0 -47.45 nM4i nM4o dump dump

# mode matching telescope on IB
s s1 0.2954 nM4o nM51 # distance from OpotoCad

# angle of incidence on M5:
# OptoCad -2.267 deg , probably wrong position of M4 ???
# Magali -4 deg 
# tuned for low astigmatism, e.g. -5.6 deg 
bs M5 1 0 0 -5.6 nM51 nM52 dump dump
attr M5 Rc -1.22
s s2 0.5495 nM52 nM61
# angle of incidence on M5:
# OptoCad 4.73 deg , probably slightly wrong position of M5 ???
# Magali 4.9 deg 
bs M6 1 0 0 4.9 nM61 nM62 dump dump
attr M6 Rc 3.16

s s3 .596 nM62 nD1

m Dia 0 1 0 nD1 nD2

s s3b 4.729 nD2 nMPR1 # distance m6-MPR is is 5.325 


######################## CENTER #####################

m MPRo 0 1 0 nMPR1 nMPR2
#attr MPRo Rc -4.276
attr MPRo Rc -4.3 # nominal Rc=4.3m
s sP 0.03 1.44963 nMPR2 nMPR3
m MPRi 0 0.0783 0 nMPR3 nMPR4


s sS 6.11325 nMPR4 nBSs

bs BS 0.5 0.5 0 -44.9646 nBSs nBSw nBSi1 nBSi3       
s sBS1 0.06299 1.44963 nBSi1 nBSi2
s sBS2 0.06299 1.44963 nBSi3 nBSi4
bs BSAR1 0 1 0 -29.238 nBSi2 dump nBSn nBSAR       
bs BSAR2 0 1 0 -29.238 nBSi4 dump nBSe dump      


################### NORTH ARM #####################

#s sNs 6.3807 nBSn nMNI1  # as in Siesta card
s sNs 6.32009 nBSn nMNI1  # as in Siesta card minus optical path of BS

m MNIAR 0 1 0 nMNI1 nMNIi1 
s sMNI .097 1.44963 nMNIi1 nMNIi2
#m MNI 0.8819765 0.118 0 nMNIi2 nMNI2   # loss 24ppm
m MNI 0.86 0.118 0 nMNIi2 nMNI2   # loss 24ppm
attr MNI xbeta -.2u

s sN  2999.9 nMNI2 nMNE1

m MNE 0.99995 50u 0 nMNE1 nMNEi1    # loss 45ppm
s sMNE .096 1.44963 nMNEi1 nMNEi2
m MNEAR 0 1 0 nMNEi2 nMNE2
attr MNE Rc 3520.50 # tuned to fit data for f_(TEM01-TEM00)
#attr MNE Rc 3602 # average curvature from phase map
attr MNE ybeta -.5u

###################### NORTH BENCH #####################

s sN2 1.77 nMNE2 nL1
lens L1 1.02 nL1 nL2
s sN3 .8996 nL2 nL3
lens L2 -.2 nL3 nL4
s sN4 .2146 nL4 nL5
lens L3 -.1 nL5 nL6
s sN5 .365 nL6 nL7
bs M71 .5 .5 0 0 nL7 nL8 nLapp1 dump
s sN6 .305 nL8 nL9 
bs M72 .5 .5 0 0 nL9 nL10 nL11 dump
s sN7 .10 nL10 nL12
lens L4a -.1 nL12 nL13
s sN8 .759 nL13 nout1
s sN9 .286 nL11 nL15
lens L4b -.1 nL15 nL16
s sN10 .832 nL16 nout2

s sLapp 0.97 nLapp1 noutB7

#pd1 Q71 $fMI 24 nout1
#pdtype Q71 x-split

#pd  B7DC noutB7
#pd pNC nMNE1
#pd pNC1 nBSn
#pd1  B7DC-90 $fMI -90 noutB7
#pd1  B7DC-60 $fMI -60 noutB7
#pd1  B7DC-30 $fMI -30 noutB7
#pd1  B7DC0 $fMI 0 noutB7
#pd1  B720 $fMI 20 noutB7
#pd1  B7DC60 $fMI 60 noutB7
#pd1  B7DC90 $fMI 90 noutB7


#====================== COMMANDS ============================

cav NC MNI nMNI2 MNE nMNE1


#xaxis MNI phi lin -100 300 2000

retrace

#bp beamNI x w nMNI1
#bp beamNE-h x w nL1
#bp beamNE-v y w nL1
#bp beamDI x w nD1
#xaxis* s2 L lin -.017 .017 400

beam b1p nBSe

xaxis b1p x lin -3 3 60
x2axis b1p y lin -3 3 60
#x2axis* s2 L lin -.01 .01 60
#x2axis b1p y lin -5 5 40


#yaxis log abs

trace 8

#pause 

gnuterm no
gnuterm no

GNUPLOT
#set view 60, 140, ,
#set hidden3d
#set cbrange[1e-6:1e4]
#set cntrparam levels 20
#set contour
#set view 0, 0, ,
#set cbrange [10:100]
set size ratio 1
set ylabel -2
set nosurface
unset hidden3d
set colorbox vertical
set colorbox user origin .9,.1 size .04,.8
#set pal noborder
#set style line 100 lt 19 lw 0 #gif1
#set cbrange [0:2200]
#set zrange [0:1500]

set style line 100 lt -1 lw 0
#set pm3d at s hidden3d 100 solid

set pm3d map
#set pm3d at s

#-----------------------Colors--------------------------------
# -- traditional pm3d (black-blue-red-yellow)
#set palette rgbformulae 7,5,15
# -- green-red-violet
#set palette rgbformulae 3,11,6
# -- ocean (green-blue-white)  try also all other permutations
#set palette rgbformulae 23,28,3
# -- hot  (black-red-yellow-white)
#set palette rgbformulae 21,22,23
# --  colour printable on gray (black-blue-violet-yellow-white)
#set palette rgbformulae 30,31,32
# -- rainbow (blue-green-yellow-red)
#set palette rgbformulae 33,13,10
# -- AFM hot (black-red-yellow-white)
#set palette rgbformulae 34,35,36
set palette gray
#-------------------------------------------------------------
set title 0,3
set format z '%.1g'
#set format y '%.1g'
#set size ratio 1
#set xlabel  0,1
#set title "blabla"
#set label "phi [deg] (m3) " at screen .05,.45 rotate
#set nokey
#set title "pow_trans"
unset grid
END
