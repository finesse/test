
#-----------------------------------------------------------------------

l i1 2.8288770625166e+02 0 nMU3in
mod  eom5 $fMI 0.27758 2 pm 0 nMU3in nMU3_3_1     # Schnupp2 (MI control)
s s0 0 nMU3_3_1 nMU3_3_2
lens lpr 1.8 nMU3_3_2 nMU3_4_1
s s1 0 nMU3_4_1 nMU3_4_2
# some rather arbitrary thermal lense for the isolators and the EOMs:
lens therm 5.2 nMU3_4_2 nMU3_5_1
s s2 0 nMU3_5_1 nMU3_5_2
isol d2 120 nMU3_5_2 nMU3out               	# Faraday Isolator

# 070502 corrected length with respect to OptoCad (Roland Schilling)
s    smcpr3 4.391 nMU3out nBDIPR1  
bs1  BDIPR 50e-6 30e-6 0 45 nBDIPR1 nBDIPR2 dump dump
s    smcpr4 0.11 nBDIPR2 nMPRo


##------------------------------------------------------------ 
## main interferometer ##
##
## Mirror specifications for the _final_ optics are used.
#
## first (curved) surface of MPR
mirror    mPRo 0 1 0 nMPRo nMPRi
#attr mPRo Rc -1.867     # Rc as specified 
attr mPRo Rc -1.85842517051051 # Rc as used in OptoCad (layout_1.41.ocd)
s    smpr 0.075 1.44963 nMPRi nPRo
# second (inner) surface of MPR
#m1   MPR 1000e-6 38e-6 0. nPRo nPRi   	# 1000ppm power recycling
#m   MPR 0 1 0. nPRo nPRi       	# no power recycling
mirror   MPR 0 1 0. nPRo nPRi       	# T=100% PR
#m   MPR 0.99895 0.001 0. nPRo nPRi       	# T=0.1% PR
#attr MPR ybeta 2u
#s    swest 1.137 nPRi nBSwest
s    swest 1.145 nPRi nBSwest
#s    swest 1.145 nBDIPR2 nBSwest	#nPRi nBSwest

## BS
##
##                              
##                       nBSnorth     ,'-.
##                             |     +    `. 
##                             |   ,'       :'
##            nBSwest          |  +i1      +
##         ---------------->    ,:._  i2 ,'
##                             + \  `-. +       nBSeast
##                           ,' i3\   ,' ---------------
##                          +      \ +
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nBSsouth
##                             -   |
##                                 |
##                                 |


bs   BS 0.4859975 0.5139975 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#bs   BS 0.5139975 0.4859975  0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#bs   BS 0.50 0.50 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#attr BS ybeta 2u
s    sBS1a 0.041 1.44963 nBSi1 nBSi1b
# here thermal lense of beam splitter (Roland: f about 1000m for 10kW at BS)
lens bst 30k nBSi1b nBSi1c
s    sBS1 0.051 1.44963 nBSi1c nBSi2
s    sBS2 0.091 1.44963 nBSi3 nBSi4
bs   BS2 50u 0.99992 0 -27.9694 nBSi2 dump nBSeast nBSAR  
bs   BS3 50u 0.99992 0 -27.9694 nBSi4 dump nBSsouth dump	

## north arm
s snorth1 598.5682 nBSnorth nMFN1
bs1 MFN 50e-6 10e-6 -0.0 0.0 nMFN1 nMFN2 dump dump
attr MFN Rc 666

s snorth2 597.0158 nMFN2 nMCN1
mirror* MCN 50 10 0.0 nMCN1 dump
attr MCN Rc 636
#attr MCN Rc 622


## east arm
s seast1 598.4497 nBSeast nMFE1
bs1 MFE 50e-6 10e-6 0.0 0.0 nMFE1 nMFE2 dump dump
attr MFE Rcx 665	# 71 W
attr MFE Rcy 662	# 71 W
#attr MFE Rc 663.75	# perfect curvature
#attr MFE Rcx 666.41	# 66W
#attr MFE Rcy 663.75	# 66W
#attr MFE Rcy 660.75	# 75W
#attr MFE Rcx 663.75	# 75W

s seast2 597.0713 nMFE2 nMCE1
mirror* MCE 50 10 0.0 nMCE1 dump  
attr MCE Rc 622
#attr MCE Rc 636


attr MFE xbeta 0u
attr MFN xbeta 0u
attr MCE ybeta 0n
attr MCN ybeta 0n


## south arm
#s ssouth 1.104 nBSsouth nMSRi
#s ssouth 1.115 nBSsouth nMSRi
s ssouth 1.098 nBSsouth nMSRi

#m MSR 0.99 0.01 0.0 nMSRi nMSRo         	# broadband
#m MSR 0.935 0.065 0.0 nMSRi nMSRo         	# broadband
#m MSR 0.993 0.006971 0.2 nMSRi nMSRo 	        # about 150 Hz detuning
#m MSR 0.99 0.01 0.7 nMSRi nMSRo   	# about 500 Hz detuning
#m MSR 0.99 0.01 -28.76 nMSRi nMSRo 	        # - 20 kHz detuning
#m MSR 0.979 0.021 -7.344 nMSRi nMSRo 	        # -5.1 kHz detuning
#m MSR 0.0 1 0.0 nMSRi nMSRo              	# No Signal recycling
#m MSR 0.979 0.021 -1.47 nMSRi nMSRo 	        # -1041 Hz pole frequency
#m MSR 0.979 0.02095 -1.47 nMSRi nMSRo 	        # -1041 Hz pole frequency
#m MSR 0.979 0.02095 1.465 nMSRi nMSRo 	# 1019 Hz peak sens frequency (MID res.)
#m MSR 0.979 0.02095 4.675 nMSRi nMSRo 	# 3248 Hz peak sens frequency
m MSR 0.979 0.02095 1.15 nMSRi nMSRo 	# 3248 Hz peak sens frequency
#m MSR 0.979 0.02095 0 nMSRi nMSRo		# tuned


##------------------------------------------------------------ 
## commands
maxtem off
time
#trace 8
phase 3
# MC1 cavity
#cav mc1 MMC1a nMC1_0 MMC1a nMC1_5
# MC2 cavity
#cav mc2 MMC2a nMC2_0 MMC2a nMC2_5
# PR cavity (north arm)
cav prc1 MPR nPRi MCN nMCN1 
# PR cavity (east arm)
cav prc2 MPR nPRi MCE nMCE1 
# SR cavity (north arm)
cav src1 MSR nMSRi MCN nMCN1 
# SR cavity (east arm)
cav src2 MSR nMSRi MCE nMCE1 

##------------------------------------------------------------ 

##pause

	
const fMI   14.904658M	#14.904915M	#889M	#950M
const phip 119	# 174
const phiq 29	# 84

## Outputs

/*
# SB amplitudes
ad MI+ $fMI nMSRo
ad MI- -$fMI nMSRo
*/

fsig sig1 snorth1 1 0
fsig sig2 snorth2 1 0
fsig sig3 seast1 1 180
fsig sig4 seast2 1 180

# Sensitivity
pdS2 out1 $fMI max 1 max nMSRo
#ad sig+ 1 nMSRo
#ad sig- -1 nMSRo
xaxis sig1 f log 100 10k 10000
put out1 f2 $x1
#xparam out1 f2 1 0
#xparam out2 f2 1 0
#xparam sig+ f 1 0
#xparam sig- f -1 0

gnuterm no
