#-----------------------------------------------------------------------
# 11ne-signal.kat            Input File for FINESSE (www.rzg.mpg.de/~adf)
#
# Same as 09ne-signal but including the tuned input
# optics (IB and BS)
#
# Computing the matrix with astigmatism,
# using low frequency transfer function (fsig)
#
#
# 07.07.2004 by  Andreas Freise (andreas.freise@ego-gw.it)
#------------------------------------------------------------ 

const fMI 6.264080M              # modultion frequency currently used
#const fMI 6.264580M              # modultion frequency optimised for simulation
const nsilica 1.44963

##################### LASER #####################

# input laser, here waist of the input modecleaner (IMC)
l i1 10 0 0 nin
gauss g1 i1 nin 4.26e-3 -42.836m # tuned to measured absolute beam sizeat NE
#gauss g1 i1 nin 5e-3 -42.836m # nomial value

################# INJECTION BENCH ###############

# output mirror of IMC
s sin 0 nin nin2
bs IMC2i 0 1 0 -45.0 nin2 dump nIMC2i1 dump
s  sIMC2 0.034 $nsilica nIMC2i1 nIMC2I2
bs IMC2o 0 1 0 -29.20 nIMC2I2 dump nIMC2o1 dump

# modulator for simplicity after IMC
s s0 1m nIMC2o1 nEO1
mod eom1 $fMI .15 1 pm 0 nEO1 nEO2

# distance from OptoCad 
s sM4 0.151 1 nEO2 nM4a

# mirror M_IB4 is currently mounted in reverse, so that the
# AR coating is in front and the beam passes the substrate
# twice.
bs M_IB4AR1 0 1 0 -46.71 nM4a dump nM4i1 dump
s sM41 12m $nsilica nM4i1 nM4i2
bs M_IB4HR 0.999 0.001 0 -30.15 nM4i2 nM4i3 dump dump 
s sM42 12m $nsilica nM4i3 nM4i4
bs M_IB4AR2 0 1 0 30.14 nM4i4 dump nM4b dump 
s s1 0.286 nM4b nM51 # distance from OpotoCad

# mode matching telescope on IB
# angle of incidence on M_IB5: OptoCad -3.3 deg, Magali -4 deg 
bs M_IB5 1 0 0 -3.3 nM51 nM52 dump dump
attr M_IB5 Rc -1.22
#s s2 0.545 nM52 nM61  # distance measured (before beam matching II)
s s2 0.541 nM52 nM61  # distance expected after beam matching II

# angle of incidence on M6: OptoCad  5.0 deg, Magali 4.9 deg 
bs M_IB6 .995 0.005 0 5.4 nM61 nM62 dump nM64i
s sMIB6 0.01 $nsilica nM64i nM65i
bs M_IB6AR 0 1 0 0.07 nM65i dump nM66 dump

attr M_IB6 Rc 3.138 # tuned value
# above: values tuned to match the measured beam sizes
# below nominal values
#bs M_IB6 1 0 0 5 nM61 nM62 dump dump 
#attr M_IB6 Rc 3.16 

s s3 .596 nM62 nD1
m Dia 0 1 0 nD1 nD2   # diaphragm 
s s3b 4.68 nD2 nMPR1  # distance M_IB6-MPR (s3+s3b) is is 5.276

################## DETECTORS TABLE ##################


s sDT1 2.5 nM66 nLDT11
lens LDT1 .5 nLDT11 nLDT12
#s sDT2 .385 nLDT12 nLDT21 # from Giordano
s sDT2 .3865 nLDT12 nLDT21  # tuned
lens LDT2 -.1 nLDT21 nLDT22
s sDT3 .3 nLDT22 nM201
bs M20 .5 .5 0 0 nM201 nM202 nM203 dump
s sDT4 .1 nM202 nLDT3a1
lens LDT3a -.1 nLDT3a1 nLDT3a2
s sDT5 .3 nLDT3a2 nQ21

s sDT6 .3 nM203 nLDT3b1
lens LDT3b -.1 nLDT3b1 nLDT3b2
s sDT7 .46 nLDT3b2 nQ22


######################## CENTER #####################

# power recycling mirror
m MPRo 0 1 0 nMPR1 nMPR2 # secondary surface, AR coated, curved
attr MPRo Rc -4.3 # nominal value 
s sP 0.03 $nsilica nMPR2 nMPR3
m MPRi 0.9217 0.0783 0 nMPR3 nMPR4

s sS 5.96896 nMPR4 nBSs # from OptoCad

# Beamsplitter
bs BS 0.5025 0.49745 0 -44.978 nBSs nBSw nBSi1 nBSi3       
s sBS1 0.0632 $nsilica nBSi1 nBSi2
s sBS2 0.0632 $nsilica nBSi3 nBSi4
bs BSAR1 0.00052 0.99943 0 -29.125 nBSi2 dump nBSn nBSAR       
bs BSAR2 0 1 0 -29.125 nBSi4 dump nBSe dump      

######################## NORTH ARM #####################

s sNs 6.19618 nBSn nMNI1  # as in OptoCad

# North Input Mirror T=11.8%, Loss=1.8%
# (one could also use e.g. T=13.6%, Loss=50ppm)
m MNIAR 0 1 0 nMNI1 nMNIi1     # secondary surface (AR coated)
s sMNI .097 $nsilica nMNIi1 nMNIi2
#m MNI 0.864 0.118 0 nMNIi2 nMNI2      
m MNI 0.882 0.118 0 nMNIi2 nMNI2      
#attr MNI ybeta 1u              # some arbritrary mis-alignment

s sNl 2999.9 nMNI2 nMNE1       # cavity length

# North End Mirror T=50ppm, Loss=50ppm
m MNE 0.9999 50u 0 nMNE1 nMNEi1   # primary surface  
s sMNE .096 $nsilica nMNEi1 nMNEi2 # fused silica substrate
m MNEAR 0 1 0 nMNEi2 nMNE2        # secondary surface (AR coated)
attr MNE Rc 3530                  # tuned radius of curvature of NE
#attr MNE xbeta -.3u               # some arbritrary mis-alignment

######################## WEST ARM #####################

#s sWs 5.5328 nBSw nMWI1           # as in Siesta card
s sWs 5.254 nBSw nMWI1            # as in OptoCad

# West Input Mirror T=11.66%, 
m MWIAR 0 1 0 nMWI1 nMWIi1        # secondary surface (AR coated)
s sMWI .097 $nsilica nMWIi1 nMWIi2
m MWI 0.883 0.1166 0 nMWIi2 nMWI2      
#attr MWI ybeta -2u               # some arbritrary mis-alignment

s sWl 2999.9 nMWI2 nMWE1          # cavity length

# West End Mirror T=50ppm, Loss=50ppm
m MWE 0.9999 50u 0.0 nMWE1 nMWEi1   
s sMWE .096 $nsilica nMWEi1 nMWEi2 # fused silica substrate
m MWEAR 0 1 0 nMWEi2 nMWE2        # secondary surface (AR coated)
#attr MWE Rc 3620                  # radius of curvature of WE
attr MWE Rc 3530                  # radius of curvature of WE
#attr MWE xbeta 2u                 # some arbritrary mis-alignment

###################### DETECTION BENCH #################


s s50 10.6 nBSAR nLDB151
lens LDB15 2.059 nLDB151 nLDB152
s s51 2.1629 nLDB152 nLDB251
lens LDB25 0.1044 nLDB251 nLDB252
s s52 1.875 nLDB252 nLDB501
lens LDB50 0.5 nLDB501 nLDB502
s s53 .10 nLDB502 nMDB511
bs MDB51 .5 .5 0 0 nMDB511 nMDB512 nMDB513 dump
s s54 .273 nMDB512 nLDB521
lens LDB52 -0.1 nLDB521 nLDB522
s s55 .435 nLDB522 nQ52 

s s56 .38 nMDB513 nLDB51a1
lens LDB51a -0.2 nLDB51a1 nLDB51a2
s s57 .08 nLDB51a2 nLDB51b1
lens LDB51b -0.1 nLDB51b1 nLDB51b2
s s58 .44 nLDB51b2 nQ51


###################### NORTH BENCH #####################



# L71 first large lens after NE
s s72 1.77 nMNE2 nL71
lens L71 1.02 nL71 nL72 

# L72 and L73, small lenses after L71
s s73 .8996 nL72 nL73
lens L72 -.2 nL73 nL74
s s74 .2146 nL74 nL75
lens L73 -.1 nL75 nL76
s s75 .365 nL76 nL77

# beam split into 50% going to B7 and 50% going to Q71, Q72
bs M71 .5 .5 0 0 nL77 nL78 nB7 dump
s sB7 0.97 nB7 noutB7

# beam split again 50/50, for Q71 and Q72
s s76 .305 nL78 nL79 
bs M72 .5 .5 0 0 nL79 nL710 nL711 dump

# lens L74a and Q71
s s77 .10 nL710 nL712
lens L4a -.1 nL712 nL713
s s78 .759 nL713 nQ71

# lens L74b Q72
s s79 .286 nL711 nL715
lens L4b -.1 nL715 nL716
s s710 .832 nL716 nQ72


###################### WEST BENCH #####################


# L81 first large lens after WE
s s82 1.77 nMWE2 nL81
lens L81 1.02 nL81 nL82 

# L82 and L83, small lenses after L81
s s83 .8996 nL82 nL83
lens L82 -.2 nL83 nL84
s s84 .2146 nL84 nL85
lens L83 -.1 nL85 nL86
s s85 .365 nL86 nL87

# beam split into 50% going to B8 and 50% going to Q81, Q82
bs M81 .5 .5 0 0 nL87 nL88 nB8 dump
s sB8 0.97 nB8 noutB8

# beam split again 50/50, for Q81 and Q82
s s86 .305 nL88 nL89 
bs M82 .5 .5 0 0 nL89 nL810 nL811 dump

# lens L84a and Q81
s s87 .10 nL810 nL812
lens L84a -.1 nL812 nL813
s s88 .759 nL813 nQ81

# lens L84b Q82
s s89 .286 nL811 nL815
lens L84b -.1 nL815 nL816
s s810 .832 nL816 nQ82

######################## Detection ##########################


######################## NORTH BENCH DIODES #################


pd2 Q711 $fMI 112 $fsig 0 nQ71 
pdtype Q711 y-split

/*
pd2 Q712 $fMI 22 $fsig 0 nQ71
pdtype Q712 y-split

pd2 Q721 $fMI -5 $fsig 0 nQ72
pd2 Q722 $fMI 85 $fsig 0 nQ72
pdtype Q721 y-split
pdtype Q722 y-split

# Power on one of the quadrants
#pd  Q71DC nQ71
#scale 0.003 Q71DC 
*/

######################## WEST BENCH DIODES #################


pd2 Q811 $fMI 112 $fsig 0 nQ81
pdtype Q811 y-split

/*
pd2 Q812 $fMI 22 $fsig 0 nQ81
pdtype Q812 y-split

pd2 Q821 $fMI -5 $fsig 0 nQ82
pd2 Q822 $fMI 85 $fsig 0 nQ82
pdtype Q821 y-split
pdtype Q822 y-split

# Power on one of the quadrants
#pd  Q81DC nQ81
#scale 0.003 Q81DC
*/

######################## DETECTION DIODES ##################


pd2 Q511 $fMI 112 $fsig 0 nQ51 
pdtype Q511 y-split

/*
pd2 Q512 $fMI 22 $fsig 0 nQ51
pdtype Q512 y-split

pd2 Q521 $fMI -5 $fsig 0 nQ52
pd2 Q522 $fMI 85 $fsig 0 nQ52
pdtype Q521 y-split
pdtype Q522 y-split
*/


pd2 Q211 $fMI 112 $fsig 0 nQ21 
pdtype Q211 y-split

/*
pd2 Q212 $fMI 22 $fsig 0 nQ21
pdtype Q212 y-split

pd2 Q221 $fMI -5 $fsig 0 nQ22
pd2 Q222 $fMI 85 $fsig 0 nQ22
pdtype Q221 y-split
pdtype Q222 y-split
*/

/*
pd B1 nBSe
pd B7 nMNE1
pd B8 nMWE1
pd BSp nBSs
pd B2 nMPR1
*/

######################## COMMANDS ##########################
const fsig .001
cav NC MNI nMNI2 MNE nMNE1     # compute north cavity
cav WC MWI nMWI2 MWE nMWE1     # compute west cavity
maxtem 1                     # order of higher order modes (n+m)
#retrace
yaxis lin abs
#pause

######################## TASKS #############################



# find dark fringe
#xaxis BS phi lin -10 10 100
#xaxis MPRi phi lin -10 10 100


/*
# Plot Gouy phase from NE to quadrants on the north external bench
gouy g71 y s72 s73 s74 s75 s76 s77 s78
gouy g72 y s72 s73 s74 s75 s76 s79 s710
# Rescale Gouy phases from rad to degree
scale DEG g71
scale DEG g72
# Plot beam size at quadrants Q71 and Q72
#bp w71 x w nQ71
#bp w72 x w nQ72
# Tuning the lens L72 by chaning the lengths of the
# spaces in front and behind the lens.
xaxis* s73 L lin -5m 5m 600
xparam s74 L -1 1.1142
*/

/*
# Plot Gouy phase from WE to quadrants on the north external bench
gouy g81 y s82 s83 s84 s85 s86 s87 s88
gouy g82 y s82 s83 s84 s85 s86 s89 s810
scale DEG g81
scale DEG g82
#bp w81 x w nQ81
#bp w82 x w nQ82
xaxis* s83 L lin -5m 5m 600
xparam s84 L -1 1.1142
*/

/*
# Plot Gouy phase from BS to quadrants on the detection (external) bench
gouy g51 y s50 s51 s52 s53 s56 s57 s58
gouy g52 y s50 s51 s52 s53 s54 s55 

scale DEG g51
scale DEG g52
#bp w51 x w nQ51
#bp w52 x w nQ52
xaxis* s51 L lin -100m 100m 600
xparam s52 L -1 4.0379
*/

/*
# Plot Gouy phase from MPR to quadrants on the detectors table
gouy g21 y s3b s3 sDT1 sDT2 sDT3 sDT4 sDT5
gouy g22 y s3b s3 sDT1 sDT2 sDT3 sDT6 sDT7

scale DEG g21
scale DEG g22
#bp w21 x w nQ21
#bp w22 x w nQ22
xaxis* sDT2 L lin -50m 50m 600
xparam sDT3 L -1 .685
*/


fsig sig1 MNI y $fsig 0
fsig sig1 MWI y $fsig 0
#fsig sig1 MNI y $fsig 0
fsig sig1 MPRi y $fsig 180
xaxis Q511 phi1 lin -180 90 100
put Q711 phi1 $x1
put Q811 phi1 $x1
put Q211 phi1 $x1
#xparam Q711 phi1 1 0
#xparam Q811 phi1 1 0
#xparam Q211 phi1 1 0

gnuterm no
