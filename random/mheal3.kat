# Structur-data: positions, orientations, directions

#H4sIAAAAAAAAAFvzloG1uIhBOCuxLFGvtCQzR8+xqCix0iezuKSi8ZLszOOJc5kZGD0ZWIozq1IrChgY
#GKLKWYBka2kRA1O0p++uBLWyV5uWMjEwgCWBNIMEkLoGlo4K71ewFNkR+wguzchQWshQB1YGwh5g
#pSARFrAsI4psBV7ZFXhlb+CTZeTAKcsI4qOolQRS51FEPOEiLJh+YswgwexEILUdQ6QdoZuBEcPs
#MiRZBgJmh2OIqOM1W4x4sxm/Y4gcx2c24zYSzJ6OIZKO1+wwEsw2xxBhx2o2pu5KIF6PIrISLoIj
#JTB8w+oykOwJ1DDBm048MWyWRLMZI32jmI0vTBjhMYcs8h2nuznQ0gmB9M1wHUNkOb64ZJhGZFwC
#w4ShHz1MoCI4wgTZbLxhchM9vJk48cU0kwbOuASZ1gDE53DJMjkAZefhCm1M2QoAnLs9dLIFAAA=

# Content-length:470

l i1 1 0  n31
s sin 0 n31 n30
const fSR   8.986595M
mod eom4 37.M .3 2 pm 0 n30 n29_1		#Schnupp1 (SR control)
s s0 0 n29_1 n29_2
mod eom5 $fSR .5 1 pm 0 n29_2 n28_1		#Schnupp2 (MI control)
s s1 0 n28_1 n28_2
lens lpr 1.8 n28_2 n27_1
s s2 0 n27_1 n27_2
lens therm 5.2 n27_2 n26_1
s s3 0 n26_1 n26_2
isol d2 120 n26_2 n25		#Faraday Isolator
s smcpr3 4.391  n25 n24
bs1 BDIPR 50e-6 30e-6 0 45 n24 n23 dump dump
s smcpr4 0.11  n23 n22
m mPRo 0 1 0 n22 n21
s smpr 0.075 1.44963 n21 n20
m MPR 0.9865 0.0135 0. n20 n19		#T=1.35% PR
s swest 1.145  n19 n15
bs BS 0.4859975 0.5139975 0.0 42.834 n15 n16 n17 n18
s sBS1a 0.041 1.44963 n17 n14
lens bst 3k n14 n13
s sBS1 0.051 1.44963 n13 n12
s sBS2 0.091 1.44963 n18 n11
bs BS2 50u 0.99992 0 -27.9694 n12 dump n10 dump
bs BS3 50u 0.99992 0 -27.9694 n11 dump n9 dump
s snorth1 598.5682  n16 n8
bs1 MFN 50e-6 10e-6 0.0 0.0 n8 n7 dump dump
s snorth2 597.0108  n7 n6
m1 MCN 50e-6 10e-6 0.0 n6 dump
s seast1 598.4497  n10 n5
bs1 MFE 50e-6 10e-6 0.0 0.0 n5 n4 dump dump
s seast2 597.0663  n4 n3
m1 MCE 50e-6 10e-6 0.0 n3 dump
s ssouth 1.247  n9 n2
m MSR 0.99 0.01 30.0 n2 n1		#broadband
pd0 west n15
pd0 south n1
beam b1 n1

gauss beam_in i1 n31 268u -550m   
attr mPRo Rc -1.85842517051051
attr MFN Rc 666
attr MCN Rc 636
attr MFE Rc 660
attr MCE Rc 622
attr MSR xbeta .5u
cav prc1 MPR n19 MCN n6
cav prc2 MPR n19 MCE n3
cav src1 MSR n2 MCE n3

#-----------------------------------------------------------------------
# some rather arbitrary thermal lense for the isolators and the EOMs:
# 070502 corrected length with respect to OptoCad (Roland Schilling)
##------------------------------------------------------------
## main interferometer ##
##
## Mirror specifications for the _final_ optics are used.
##
# first (curved) surface of MPR
#attr mPRo Rc -1.867     # Rc as specified
# second (inner) surface of MPR
#m1   MPR 1000e-6 38e-6 0. nPRo nPRi   	# 1000ppm power recycling
#m   MPR 0 1 0. nPRo nPRi       	# no power recycling
#attr MPR ybeta 2u
## BS
##
##
##                       nBSnorth     ,'-.
##                             |     +    `.
##                             |   ,'       :'
##            nBSwest          |  +i1      +
##         ---------------->    ,:._  i2 ,'
##                             + \  `-. +       nBSeast
##                           ,' i3\   ,' ---------------
##                          +      \ +
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nBSsouth
##                             -   |
##                                 |
##                                 |
#bs   BS 0.5139975 0.4859975  0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#bs   BS 0.50 0.50 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#attr BS ybeta 2u
# here thermal lense of beam splitter (Roland: f about 1000m for 10kW at BS)
## north arm
## east arm
#attr MFE Rc 687
#attr MFE xbeta 0.9u
#attr MFN xbeta -1.u
#attr MCE ybeta 0.1u
#attr MCN ybeta -1.1u
## south arm
#m MSR 0.993 0.006971 0.2 nMSRin nMSRo 	        # about 150 Hz detuning
#m MSR 0.98 0.006971 0.7 nMSRin nMSRo   	# about 500 Hz detuning
#m MSR .0 1. 0.0 nMSRi nMSRo              	# No Signal recycling
##------------------------------------------------------------
## commands
maxtem 2
#trace 7
phase 3
# MC1 cavity
#cav mc1 MMC1a nMC1_0 MMC1a nMC1_5
# MC2 cavity
#cav mc2 MMC2a nMC2_0 MMC2a nMC2_5
# PR cavity (north arm)
# PR cavity (east arm)
# SR cavity (north arm)
# SR cavity (east arm)
##------------------------------------------------------------
## Outputs
#beam b1 nPRi
#pd refl1 nMPRo
#pd refl2 nMPRo*
#pd west nBSwest
#pd north nBSnorth
#pd east nBSeast
#pd south nMSRo
#xaxis MPR phi lin -10 10 100
#ad ad1 14.904962M nMSRo
#ad ad2 -14M nMSRo
#xaxis eom5 f lin 14.903M 14.906M 500
#xparam ad1 f 1 0
#xparam ad2 f -1 0
#pd pow nBSAR
#pd2 out1 14.904962M 20 1 max nBSAR
#pd2 out1 14.904962M 0 1 0 nMSRo
#pd1 out2 14.904962M 30 nMSRo
#pd1 out3 14.904962M 60 nMSRo
#pd1 out4 14.904962M 90 nMSRo
#xaxis MCN phi lin -3 3 100
#xparam MCE phi -1 0
#pd1 pdh1 37M -300 nMPRo
#pd1 pdh2 37M 0 nMPRo
#pd1 pdh3 37M 30 nMPRo
#pd1 pdh4 37M 60 nMPRo
#fsig f1 MCN 1 0
#fsig f1 MCE 1 180
#xaxis MFE Rc lin 650 687 100
#xaxis MSR t log 0.01 1 200
#xparam MSR r -1 1
#xaxis MPR phi lin -30 30 80
##------------------------------------------------------------
# x/yaxis for beam analyser
#beam b1 nMSRo
xaxis b1 x lin -3 3 80
x2axis b1 y lin -3 3 80
yaxis lin abs
gnuterm no                   # gnuplot terminal: x11
gnuterm no
gnuterm no                  # gnuplot terminal: x11
GNUPLOT
#set view 60, 140, ,
#set hidden3d
#set cbrange[1e-6:1e4]
#set cntrparam levels 20
#set contour
#set view 0, 0, ,
#set cbrange [0:4000]
set nosurface
unset hidden3d
set colorbox vertical
set colorbox user origin .9,.1 size .04,.8
#set pal noborder
#set style line 100 lt 19 lw 0 #gif1
#set cbrange [0:2200]
#set zrange [0:1500]
set style line 100 lt -1 lw 0
#set pm3d at s hidden3d 100 solid
set pm3d map
#set pm3d at s
#-----------------------Colors--------------------------------
# -- traditional pm3d (black-blue-red-yellow)
#set palette rgbformulae 7,5,15
# -- green-red-violet
#set palette rgbformulae 3,11,6
# -- ocean (green-blue-white)  try also all other permutations
#set palette rgbformulae 23,28,3
# -- hot  (black-red-yellow-white)
#set palette rgbformulae 21,22,23
# --  colour printable on gray (black-blue-violet-yellow-white)
#set palette rgbformulae 30,31,32
# -- rainbow (blue-green-yellow-red)
#set palette rgbformulae 33,13,10
# -- AFM hot (black-red-yellow-white)
#set palette rgbformulae 34,35,36
set palette gray
#-------------------------------------------------------------
set title 0,3
set format z '%.1g'
#set format y '%.1g'
set size ratio 1
#set xlabel  0,1
#set title "blabla"
#set label "phi [deg] (m3) " at screen .05,.45 rotate
#set nokey
#set title "pow_trans"
unset grid
END

