#-----------------------------------------------------------------------
# m6.kat                  Input File for FINESSE (www.rzg.mpg.de/~adf)
#
# effects of a new M6 on the matching
#
# 1. nominal ROC 3160 mm
#    check for 3200 mm (and also 3000 - 3200 mm)
# 
# 2. how much displacement required for 3200 to re-match beam
#
#
#
# 01.06.2004 by  Andreas Freise (andreas.freise@ego-gw.it)
#------------------------------------------------------------ 

const fMI 6.264080M              # modultion frequency currently used
#const fMI 6.264580M              # modultion frequency optimised for simulation

##################### LASER #####################

# input laser, here waist of the input modecleaner (IMC)
l i1 6 0 0 nlaser
s sin 0 nlaser nin
gauss g1 i1 nlaser 4.26e-3 -42.836m # tuned to measured absolute beam sizeat NE
#gauss g1 i1 nlaser 5e-3 -42.836m # nomial value


# output mirror of IMC
bs IMC2i 0 1 0 -45.0 nin dump nIMC2i1 dump
s  sIMC2 0.034 1.44963 nIMC2i1 nIMC2I2
bs IMC2o 0 1 0 -29.20 nIMC2I2 dump nIMC2o1 dump

# modulator for simplicity after IMC
s s0 1m nIMC2o1 nEO1
mod eom1 $fMI .1 1 pm 0 nEO1 nEO2

# distance from OptoCad 
s sM4 0.151 1 nEO2 nM4a

# mirror M_IB4 is currently mounted in reverse, so that the
# AR coating is in front and the beam passes the substrate
# twice.
bs M_IB4AR1 0 1 0 -46.71 nM4a dump nM4i1 dump
s sM41 12m 1.44963 nM4i1 nM4i2
bs M_IB4HR 0.999 0.001 0 -30.15 nM4i2 nM4i3 dump dump 
s sM42 12m 1.44963 nM4i3 nM4i4
bs M_IB4AR2 0 1 0 30.14 nM4i4 dump nM4b dump 
s s1 0.286 nM4b nM51 # distance from OpotoCad

# mode matching telescope on IB
# angle of incidence on M_IB5: OptoCad -3.3 deg, Magali -4 deg 
bs M_IB5 1 0 0 -3.3 nM51 nM52 dump dump
#bs M_IB5 1 0 0 0 nM51 nM52 dump dump  # without astigmatism
attr M_IB5 Rc -1.22

s s2 0.541 nM52 nM61  # distance measured (before beam matching II)

# angle of incidence on M_IB6: OptoCad  5.0 deg, Magali 4.9 deg 
bs M_IB6 1 0 0 5.4 nM61 nM62 dump dump
#bs M_IB6 1 0 0 0 nM61 nM62 dump dump without astigmatism
attr M_IB6 Rc 3.2 # tuned value
#attr M_IB6 Rc 3.138 # tuned value
# above: values tuned to match the measured beam sizes
# below nominal values
#bs M_IB6 1 0 0 5 nM61 nM62 dump dump 
#attr M_IB6 Rc 3.16 

s s3 .596 nM62 nD1
m Dia 0 1 0 nD1 nD2   # diaphragm 
s s3b 4.68 nD2 nMPR1  # distance M_IB6-MPR (s3+s3b) is is 5.276

######################## CENTER #####################

# power recycling mirror
m MPRo 0 1 0 nMPR1 nMPR2 # secondary surface, AR coated, curved
attr MPRo Rc -4.3 # nominal value 
s sP 0.03 1.44963 nMPR2 nMPR3
m MPRi 0 0.0783 0 nMPR3 nMPR4

s sS 5.96896 nMPR4 nBSs # from OptoCad

# Beamsplitter
bs BS 0.5025 0.49745 0 -44.978 nBSs nBSw nBSi1 nBSi3       
s sBS1 0.0632 1.44963 nBSi1 nBSi2
s sBS2 0.0632 1.44963 nBSi3 nBSi4
bs BSAR1 0 1 0 -29.125 nBSi2 dump nBSn nBSAR       
bs BSAR2 0 1 0 -29.125 nBSi4 dump nBSe dump      

######################## NORTH ARM #####################

s sNs 6.19618 nBSn nMNI1  # as in OptoCad

# North Input Mirror T=11.8%, Loss=1.8%
# (one could also use e.g. T=13.6%, Loss=50ppm)
m MNIAR 0 1 0 nMNI1 nMNIi1     # secondary surface (AR coated)
s sMNI .097 1.44963 nMNIi1 nMNIi2
m MNI 0.864 0.118 0 nMNIi2 nMNI2      
attr MNI ybeta 1u              # some arbritrary mis-alignment

s sNl 2999.9 nMNI2 nMNE1       # cavity length

# North End Mirror T=50ppm, Loss=50ppm
m MNE 0.9999 50u 0 nMNE1 nMNEi1   # primary surface  
s sMNE .096 1.44963 nMNEi1 nMNEi2 # fused silica substrate
m MNEAR 0 1 0 nMNEi2 nMNE2        # secondary surface (AR coated)
attr MNE Rc 3530                  # tuned radius of curvature of NE
attr MNE xbeta -.3u               # some arbritrary mis-alignment

###################### NORTH BENCH #####################

s sN2 1.77 nMNE2 nNB1             # distance MNE-L1
lens L1 1.02 nNB1 nNB3            
s sB7 1 nNB3 noutB7               # arbitrary distance to B7

#----------------------------------------------------------------
# the following commands cannot be used at the same
# time. The file should be used three times with 
# one set of commands uncommented each time.

# 1. power transmitted by the north arm cavity
# as a function of the telescope length.
# We get a maximum coupling at approx. -6mm.
# (Here it might be useful to play with the `maxtem'
# command.) 
# In addition, we use `trace 8'
# to print the beam parameters at the NI and
# the lens L1 to compare them to the beam size later:
# 10: node nMNI1(31); MNIAR(3),sNs(30); n=1  (MNIAR --> nMNI1)
#   x,y: w0=20.666266mm w=20.666266mm z=66.913626mm z_R=1.26105km
# 5: node nNB1(39); sN2(34),L1(36); n=1  (sN2 --> nNB1)
#    x,y: w0=14.852735mm w=53.370079mm z=2.2480588km z_R=651.36008m
cav NC MNI nMNI2 MNE nMNE1        # compute cavity parameters
maxtem 4                          # order of TEM modes (n+m)
pd0 B7 noutB7                     # transmitted light power
#trace 8                           # print beam parameters

# 2. beam size at the north end NE (at lens L1)
# No 'cav' command here! Thus the beam parameter from
# the input beam is propageted everywhere and
# we can measure it with a `bp' detector. 
# (Please note that the light power in this case will not be 
# computed correctly!) 
# The beam sizes are equal (i.e. the beam is circular at -4mm.
# But the absolute size is approx. 61mm, i.e. more than
# 10% bigger than the cavity mode size (53mm).
#maxtem 0                          # order of TEM modes (n+m)
#bp wNEx x w nNB1                  # beam size at the lens L1 (horizontal)
#bp wNEy y w nNB1                  # beam size at the lens L1 (vertical)

# 3. beam size at the north input NI 
# (No measurements of th beam size at this location
# have been done but the reflection of NI as seen
# after some imaging shows an astigmatic beam).
# No 'cav' command here!
# The beam is astigmatic for all telescope length
# with an assysmetry of 2% (smaller in horizontal).
#maxtem 0                          # order of TEM modes (n+m)
#bp wNIx x w nMNI1                 # beam size at NI (horizontal)
#bp wNIy y w nMNI1                 # beam size at NI (vertical)
#----------------------------------------------------------------

xaxis* s2 L lin -20m 20m 100        # tuning the distance between M_IB5 and M_IB6
#                                   # on the IB (i.e. the telescope length)



retrace                            # recompute beam parameters for each data point



yaxis lin abs
#pause
gnuterm no

