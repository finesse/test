# doublecavity.kat - by Gabriele Vajente vajente@sns.it
#
# Simulations of a double Fabry-Perot cavity locked on
# resonance and with several misalignment.
#
# The purpose is to look at the sidebands behavior.


# Constants
const finput 0

const fmod   6263356
const mfmod -6263356

const midx 0.2

#################### INPUT BEAM ####################

# input laser
l i1 1 $finput 0 nin
s s0 1m nin nEOM1

# EOM
mod eom1 $fmod $midx 1 pm 0 nEOM1 nINPUT

s s1 1m nINPUT nMPRi

# gaussian modes up to TEM m+n = 8
maxtem 8

################## DOUBLE CAVITY ###################

# power recycling mirror
m MPR $RPR $TPR 90 nMPRi nMPRo

# recycling cavity length
s sPR $SPR nMPRo nMNIi

# input mirror
m MNI $RNI $TNI 0 nMNIi nMNIo 

# cavity length
s sNl $SNC nMNIo nMNEi

# end mirror
m MNE $RNE $TNE 0 nMNEi nMNEo
attr MNE Rc $NERC

# Constants (same as Laval simulations)
const RPR 0.92168
const TPR 0.0783

const RNI   0.88199
const TNI   0.118

const RNE   0.99995
const TNE   42.9u

const NERC 3583.126     

const SNC 2999.9
const SPR 12.091

# compute cavity eigenmodes

cav NC MNI nMNIo MNE nMNEi

################### BEAM IMAGES ###################

# misalign NI

attr MNI xbeta 20n ## tag run1: activated
beam image 0  nMNEo ## tag run1: activated
## tag run2: disabled ## attr MNI xbeta 20n
## tag run2: disabled ## beam image $fmod nMNEo
## tag run3: disabled ## attr MNI xbeta 20n
## tag run3: disabled ## beam image $mfmod nMNEo

## tag run4: disabled ## attr MNI xbeta 20n
## tag run4: disabled ## beam image 0  nMNEi
## tag run5: disabled ## attr MNI xbeta 20n
## tag run5: disabled ## beam image $fmod nMNEi
## tag run6: disabled ## attr MNI xbeta 20n
## tag run6: disabled ## beam image $mfmod nMNEi

## tag run7: disabled ## attr MNI xbeta 20n
## tag run7: disabled ## beam image 0  nMNIo
## tag run8: disabled ## attr MNI xbeta 20n
## tag run8: disabled ## beam image $fmod nMNIo
## tag run9: disabled ## attr MNI xbeta 20n
## tag run9: disabled ## beam image $mfmod nMNIo

## tag run10: disabled ## attr MNI xbeta 20n
## tag run10: disabled ## beam image 0  nMNIi
## tag run11: disabled ## attr MNI xbeta 20n
## tag run11: disabled ## beam image $fmod nMNIi
## tag run12: disabled ## attr MNI xbeta 20n
## tag run12: disabled ## beam image $mfmod nMNIi

## tag run13: disabled ## attr MNI xbeta 20n
## tag run13: disabled ## beam image 0  nMPRo
## tag run14: disabled ## attr MNI xbeta 20n
## tag run14: disabled ## beam image $fmod nMPRo
## tag run15: disabled ## attr MNI xbeta 20n
## tag run15: disabled ## beam image $mfmod nMPRo

## tag run16: disabled ## attr MNI xbeta 20n
## tag run16: disabled ## beam image 0  nMPRi
## tag run17: disabled ## attr MNI xbeta 20n
## tag run17: disabled ## beam image $fmod nMPRi
## tag run18: disabled ## attr MNI xbeta 20n
## tag run18: disabled ## beam image $mfmod nMPRi

# misalign NE

## tag run19: disabled ## attr MNE xbeta 20n
## tag run19: disabled ## beam image 0  nMNEo
## tag run20: disabled ## attr MNE xbeta 20n
## tag run20: disabled ## beam image $fmod nMNEo
## tag run21: disabled ## attr MNE xbeta 20n
## tag run21: disabled ## beam image $mfmod nMNEo

## tag run22: disabled ## attr MNE xbeta 20n
## tag run22: disabled ## beam image 0  nMNEi
## tag run23: disabled ## attr MNE xbeta 20n
## tag run23: disabled ## beam image $fmod nMNEi
## tag run24: disabled ## attr MNE xbeta 20n
## tag run24: disabled ## beam image $mfmod nMNEi

## tag run25: disabled ## attr MNE xbeta 20n
## tag run25: disabled ## beam image 0  nMNIo
## tag run26: disabled ## attr MNE xbeta 20n
## tag run26: disabled ## beam image $fmod nMNIo
## tag run27: disabled ## attr MNE xbeta 20n
## tag run27: disabled ## beam image $mfmod nMNIo

## tag run28: disabled ## attr MNE xbeta 20n
## tag run28: disabled ## beam image 0  nMNIi
## tag run29: disabled ## attr MNE xbeta 20n
## tag run29: disabled ## beam image $fmod nMNIi
## tag run30: disabled ## attr MNE xbeta 20n
## tag run30: disabled ## beam image $mfmod nMNIi

## tag run31: disabled ## attr MNE xbeta 20n
## tag run31: disabled ## beam image 0  nMPRo
## tag run32: disabled ## attr MNE xbeta 20n
## tag run32: disabled ## beam image $fmod nMPRo
## tag run33: disabled ## attr MNE xbeta 20n
## tag run33: disabled ## beam image $mfmod nMPRo

## tag run34: disabled ## attr MNE xbeta 20n
## tag run34: disabled ## beam image 0  nMPRi
## tag run35: disabled ## attr MNE xbeta 20n
## tag run35: disabled ## beam image $fmod nMPRi
## tag run36: disabled ## attr MNE xbeta 20n
## tag run36: disabled ## beam image $mfmod nMPRi

# misalign PR

## tag run37: disabled ## attr MPR xbeta 20n
## tag run37: disabled ## beam image 0  nMNEo
## tag run38: disabled ## attr MPR xbeta 20n
## tag run38: disabled ## beam image $fmod nMNEo
## tag run39: disabled ## attr MPR xbeta 20n
## tag run39: disabled ## beam image $mfmod nMNEo

## tag run40: disabled ## attr MPR xbeta 20n
## tag run40: disabled ## beam image 0  nMNEi
## tag run41: disabled ## attr MPR xbeta 20n
## tag run41: disabled ## beam image $fmod nMNEi
## tag run42: disabled ## attr MPR xbeta 20n
## tag run42: disabled ## beam image $mfmod nMNEi

## tag run43: disabled ## attr MPR xbeta 20n
## tag run43: disabled ## beam image 0  nMNIo
## tag run44: disabled ## attr MPR xbeta 20n
## tag run44: disabled ## beam image $fmod nMNIo
## tag run45: disabled ## attr MPR xbeta 20n
## tag run45: disabled ## beam image $mfmod nMNIo

## tag run46: disabled ## attr MPR xbeta 20n
## tag run46: disabled ## beam image 0  nMNIi
## tag run47: disabled ## attr MPR xbeta 20n
## tag run47: disabled ## beam image $fmod nMNIi
## tag run48: disabled ## attr MPR xbeta 20n
## tag run48: disabled ## beam image $mfmod nMNIi

## tag run49: disabled ## attr MPR xbeta 20n
## tag run49: disabled ## beam image 0  nMPRo
## tag run50: disabled ## attr MPR xbeta 20n
## tag run50: disabled ## beam image $fmod nMPRo
## tag run51: disabled ## attr MPR xbeta 20n
## tag run51: disabled ## beam image $mfmod nMPRo

## tag run52: disabled ## attr MPR xbeta 20n
## tag run52: disabled ## beam image 0  nMPRi
## tag run53: disabled ## attr MPR xbeta 20n
## tag run53: disabled ## beam image $fmod nMPRi
## tag run54: disabled ## attr MPR xbeta 20n
## tag run54: disabled ## beam image $mfmod nMPRi

pause
gnuterm no

GNUPLOT
set nosurface
set nocolorbox
set pm3d map
set palette gray
unset title
#set format z '%.1g'
set size ratio 1
unset grid
END

# Compute beam image

xaxis  image x lin -5 5 500
x2axis image y lin -5 5 500
