#-----------------------------------------------------------------------
# 05IB.kat                  Input File for FINESSE (www.rzg.mpg.de/~adf)
#
# Linear alignment for the VIRGO north cavity
# 
# Adding the optics of the injection bench (IB) and of
# the central part (i.e. PR, BS)
#
# The distances and angles for the setup between 
# IMC2, M4, M5 and M6 have been taken from OptoCad
# (VirgoLayout_1_6).
# 
# surfaces: IMC2 -45.0171  beams: IMC2 8.4e-3, intern -15.804
#           MIB4 -46.7000         MIB4 8.4e-3, intern -16.5503, 103.1603            
#           MIB5  83.2800         MIB5 86.5916
#           MIB6 264.9378         MIB6 -100.0805
#
# -> angles of incidence
#
# IMC2 extern 45.003  intern  29.2047
# MIB4 extern 46.7084 intern1 30.1497 intern2 -30.1397
# MIB5 3.3116
# MIB6 -5.0183
#
# Also the distances inside beam splitter substrates
# have been derived using OptoCad.
#
# 17.03.2004 by  Andreas Freise (andreas.freise@ego-gw.it)
#------------------------------------------------------------ 

const fMI 6.264080M              # modultion frequency currently used
#const fMI 6.264580M              # modultion frequency optimised for simulation

##################### LASER #####################

# input laser, here waist of the input modecleaner (IMC)
l i1 6 0 0 nin
gauss g1 i1 nin 4.26e-3 -42.836m # tuned to measured absolute beam sizeat NE
#gauss g1 i1 nin 5e-3 -42.836m # nomial value

s sin 0 nin nIMC2in

# output mirror of IMC
bs IMC2i 0 1 0 -45.0 nIMC2in dump nIMC2i1 dump
s  sIMC2 0.034 1.44963 nIMC2i1 nIMC2I2
bs IMC2o 0 1 0 -29.20 nIMC2I2 dump nIMC2o1 dump

# modulator for simplicity after IMC
s s0 1m nIMC2o1 nEO1
mod eom1 $fMI .1 1 pm 0 nEO1 nEO2

# distance from OptoCad 
s sM4 0.151 1 nEO2 nM4a

# gauss g2 derived from g1, just to force mode mismatch 
# to be computed on M4
#gauss g2 sM4 nM4i 5.0000014m 132.7421m 5.000009m 139.85894m

# mirror M4 is currently mounted in reverse, so that the
# AR coating is in front and the beam passes the substrate
# twice.
bs M4AR1 0 1 0 -46.71 nM4a dump nM4i1 dump
s sM41 12m 1.44963 nM4i1 nM4i2
bs M4HR 0.999 0.001 0 -30.15 nM4i2 nM4i3 dump dump 
s sM42 12m 1.44963 nM4i3 nM4i4
bs M4AR2 0 1 0 30.14 nM4i4 dump nM4b dump 

# mode matching telescope (1) on IB
s s1 0.286 nM4b nM51 # distance from OpotoCad

# angle of incidence on M5: OptoCad -3.3 deg, Magali -4 deg 
bs M5 1 0 0 -3.3 nM51 nM52 dump dump
attr M5 Rc -1.22

# mode matching telescope (2) on IB
s s2 0.545 nM52 nM61  # distance measured (before beam matching II)
# angle of incidence on M6: OptoCad  5.0 deg, Magali 4.9 deg 
bs M6 1 0 0 5.4 nM61 nM62 dump dump
#bs M6 1 0 0 5 nM61 nM62 dump dump
#attr M6 Rc 3.16
attr M6 Rc 3.138


s s3 .596 nM62 nD1

m Dia 0 1 0 nD1 nD2   # diaphragm (i.e aperture) 

s s3b 4.68 nD2 nMPR1 # distance M6-MPR (s3+s3b) is is 5.276

######################## CENTER #####################

# power recycling mirror
m MPRo 0 1 0 nMPR1 nMPR2 # secondary surface, AR coated, curved
#attr MPRo Rc -4.28 # tuned to fit beam measurement at NE
attr MPRo Rc -4.3 # nominal value 
s sP 0.03 1.44963 nMPR2 nMPR3
m MPRi 0 0.0783 0 nMPR3 nMPR4

#s sS 6.11325 nMPR4 nBSs # ?
s sS 5.96896 nMPR4 nBSs # from OptoCad

# Beamsplitter
bs BS 0.5025 0.49745 0 -44.978 nBSs nBSw nBSi1 nBSi3       
s sBS1 0.0632 1.44963 nBSi1 nBSi2
s sBS2 0.0632 1.44963 nBSi3 nBSi4
bs BSAR1 0 1 0 -29.125 nBSi2 dump nBSn nBSAR       
bs BSAR2 0 1 0 -29.125 nBSi4 dump nBSe dump      

######################## NORTH ARM #####################

#s sNs 6.3807 nBSn nMNI1  # as in Siesta card
#s sNs 6.32009 nBSn nMNI1 # as in Siesta card minus optical path of BS
s sNs 6.19618 nBSn nMNI1  # as in OptoCad

# North Input Mirror T=11.8%, Loss=1.8%
# (one could also use e.g. T=13.6%, Loss=50ppm)
m MNIAR 0 1 0 nMNI1 nMNIi1     # secondary surface (AR coated)
s sMNI .097 1.44963 nMNIi1 nMNIi2
m MNI 0.864 0.118 0 nMNIi2 nMNI2      
attr MNI ybeta 1u              # some arbritrary mis-alignment

s sNl 2999.9 nMNI2 nMNE1       # cavity length

# North End Mirror T=50ppm, Loss=50ppm
m MNE 0.9999 50u 0 nMNE1 nMNEi1   # primary surface  
s sMNE .096 1.44963 nMNEi1 nMNEi2 # fused silica substrate
m MNEAR 0 1 0 nMNEi2 nMNE2        # secondary surface (AR coated)
attr MNE Rc 3530                  # tuned radius of curvature of NE
attr MNE xbeta -.3u               # some arbritrary mis-alignment

###################### NORTH BENCH #####################

s sN2 1.77 nMNE2 nNB1             # distance MNE-L1
lens L1 1.02 nNB1 nNB3            
s sB7 1 nNB3 noutB7               # arbitrary distance to B7

#pd0 B7 noutB7                     # transmitted light power

bp wNEx x w nNB1                    # beam size at the lens L1 (horizontal)
bp wNEy y w nNB1                    # beam size at the lens L1 (vertical)

#bp wNIx x w nMNI1                    # beam size at NI (horizontal)
#bp wNIy y w nMNI1                    # beam size at NI (vertical)

xaxis* s2 L lin -10m 2m 100        # tuning the distance between M5 and M6
                                   # on the IB
#xaxis* s3 L lin -10m 10m 100

#cav NC MNI nMNI2 MNE nMNE1       # compute cavity parameters
maxtem 0                          # order of TEM modes (n+m)
retrace                           # recompute beam parameters for each data point
#trace 8



yaxis lin abs
#pause
gnuterm no

startnode nin

GNUPLOT
#set view 60, 140, ,
#set hidden3d
#set cbrange[1e-6:1e4]
#set cntrparam levels 20
#set contour
#set view 0, 0, ,
set nosurface
unset hidden3d
#set colorbox v
#set colorbox user origin .95,.1 size .04,.8
#set pal noborder
#set style line 100 lt 19 lw 0 #gif1
set style line 100 lt -1 lw 0
#set pm3d at s hidden3d 100 solid

set pm3d map
#set pm3d at s

#-----------------------Colors--------------------------------
# -- traditional pm3d (black-blue-red-yellow)
set palette rgbformulae 7,5,15
# -- green-red-violet
#set palette rgbformulae 3,11,6
# -- ocean (green-blue-white)  try also all other permutations
#set palette rgbformulae 23,28,3
# -- hot  (black-red-yellow-white)
#set palette rgbformulae 21,22,23
# --  colour printable on gray (black-blue-violet-yellow-white)
#set palette rgbformulae 30,31,32
# -- rainbow (blue-green-yellow-red)
#set palette rgbformulae 33,13,10
# -- AFM hot (black-red-yellow-white)
#set palette rgbformulae 34,35,36
#-------------------------------------------------------------

#set format z '%.1g'
#set format y '%.1g'

#set xlabel  0,1
#set title "blabla"
#set label "phi [deg] (m3) " at screen .05,.45 rotate
#set nokey
#set title "pow_trans"
unset grid
END
