#-----------------------------------------------------------------------
# 02fringes.kat     Input File for FINESSE (www.rzg.mpg.de/~adf)
#
# Fringes seen in the transmitted power of the north
# cavity. The output of this file should be matched
# measured data by "tuning" the simulation.
# The parameters to tune are:
# - losses (i.e. the finesse) to adjust to the shape of
#   the main fringe
# - mode mismatch and misalignemnt to match the 
#   resoances of higher order modes
# - radius of curvature of NE or the length of the
#   cavity to match the separation between the 
#   resonance peaks
# In order to prepare the use of mode mismatch and 
# misalignment first the secondary surfaces are
# introduced to the cavity mirrors.
#
# The current file represents the "tuned" state.
# The un-tuned parameters are present as comments:
# - beam parameter at input light (i.e. no gauss command)
# - MNE Rc
# - reflectivity of MNI
# 
# 16.03.2004 by  Andreas Freise (andreas.freise@ego-gw.it)
#------------------------------------------------------------ 


l i1 1 0 0 nin                 # dummy input light: 1W 

# cavity parameters (tuned simulation):
# Finesse : 42.93, Round-trip loss: 0.1360864
# opt. length: 5.9998km, FSR: 49.967075kHz, FWHM: 1.1638317kHz

s sNs 6 nin nMNI1              # arbitrary distance to NI

# untuned: no extra gauss command, below tuned
gauss g1 MNIAR nMNI1 14.8m -4  # creating a mode mismatch

# North Input Mirror T=11.8%, Loss=50ppm
m MNIAR 0 1 0 nMNI1 nMNIi1         # secondary surface (AR coated)
s sMNI .097 1.44963 nMNIi1 nMNIi2  # fused silica substrate
#m MNI 0.88195 0.118 0 nMNIi2 nMNI2 # primary surface

# above: untuned, below tuned: adding 1.8% more losses ->  Finesse = 43
#m MNI 0.864 0.118 0 nMNIi2 nMNI2      
m MNI 0.864 0.118 0 nMNIi2 nMNI2      

attr MNI ybeta 1u              # some arbritrary mis-alignment

s sN1 2999.9 nMNI2 nMNE1       # cavity length
# above: nominal length, below 
# possible length adaption to keep nominal MNE Rc
#s sN1 3062 nMNI2 nMNE1

m MNE 0.9999 50u 0 nMNE1 nMNEi1   # primary surface  
s sMNE .096 1.44963 nMNEi1 nMNEi2 # fused silica substrate
m MNEAR 0 1 0 nMNEi2 nMNE2        # secondary surface (AR coated)

#attr MNE Rc 3600
# above: untuned, below tuned
attr MNE Rc 3530

attr MNE xbeta -.3u            # some arbritrary mis-alignment

pd B7 nMNE2

xaxis MNI phi lin -100 300 2000

#pause

cav NC MNI nMNI2 MNE nMNE1     # compute cavity
maxtem 3                       # order of higher order modes (n+m)

trace 10                       # plot beam parameter for every node
                               # and cavity parameters
gnuterm no

