#-----------------------------------------------------------------------
# matrix-max1.kat            Input File for FINESSE (www.rzg.mpg.de/~adf)
#
# based on fv211204.kat and find-op.kat
#
# With maxtem 1 the error point is at phi=0 for all
# mirrors.
#
# 250105
#------------------------------------------------------------ 

retrace off

#const fMI 6.264150M                      # modultion frequency currently used
const fMI 6.264080M                      # modultion frequency as in Siesta
#const fMI 6.264580M                     # modultion frequency optimised for 
                                       # north cavity as tuned in this simulation
const nsilica 1.44963
const midx .15                           # as measured by Vincenzo

##################### LASER #####################

# input laser, here waist of the input modecleaner (IMC)
l i1 80 0 0 nin
#l i1 80 501120035 0 nin
s s0 1m nin nEO1
mod eom1 $fMI $midx 1 pm 0 nEO1 nEO2
s s1 1m nEO2 nInj

bs Inj .1 .9 0 0 nInj nMPR0 dump nDT1   # simulate T=.9 of new M_IB6 
s sDT0 1m nDT1 nDT2
lens M6  -1.581 nDT2 nM66                # M_IB6 lens effect with f=R/2

######################## CENTER #####################

s sMPR 5.276 nMPR0 nMPR1                  # distance M_IB6-MPR 

# gaussian beam parameters with astigmatism given by the
# injection bench telescope
gauss* gaussPR MPRo nMPR2  -697.792 1392.63 710.53 1414.31  
startnode nMPR2                 

# MPR power recycling mirror
m MPRo 0 1 0 nMPR1 nMPR2               # secondary surface, AR coated, curved
attr MPRo Rc -4.3                      # nominal value 
s sP 0.03 $nsilica nMPR2 nMPR3
m MPR 0.0 0.0783 0 nMPR3 nMPR4        # no recycling to simulate "far misaligned"
#m MPR 0.9217 0.0783 0 nMPR3 nMPR4      # nominal values

s sS 6.0835  nMPR4 nBSs # tuned for L_PRC=12.073m

# BS beamsplitter
bs BS 0.5025 0.49745 0 -44.978 nBSs nBSw nBSn nBSe       
#s sBS1 0.0632 $nsilica nBSi1 nBSi2
#s sBS2 0.0632 $nsilica nBSi3 nBSi4
#bs BSAR1 0.00052 0.99943 0 -29.125 nBSi2 dump nBSn nBSAR       
#bs BSAR2 0 1 0 -29.125 nBSi4 dump nBSe dump      

######################## NORTH ARM #####################

s sNs 6.19618 nBSn nMNI1  

m MNIAR 132u 0.999868 0 nMNI1 nMNIi1   # secondary surface (AR coated)
#m MNIAR 0 0 0 nMNI1 nMNIi1            # North arm "misaligned"
s sMNI .097 $nsilica nMNIi1 nMNIi2
#m MNI 0.881 0.118 0 nMNIi2 nMNI2       # 0.1% extra losses
m MNI 0.882 0.118 0 nMNIi2 nMNI2      # nominal values


s sNl 2999.9 nMNI2 nMNE1               # cavity length

# North End Mirror T=50ppm, Loss=50ppm
m MNE 0.9999 42.9u 0 nMNE1 nMNEi1      # primary surface  
s sMNE .096 $nsilica nMNEi1 nMNEi2     # fused silica substrate
m MNEAR 0 1 0 nMNEi2 nMNE2             # secondary surface (AR coated)
attr MNE Rc 3530                       # tuned radius of curvature of NE

######################## WEST ARM #####################

#s sWs 5.41 nBSw nMWI1                  # tuned for Mi asssym=0.878
s sWs 5.34828 nBSw nMWI1                  # tuned for Mi asssym=0.8479 (no BS substrate)

# West Input Mirror T=11.66%, 
#m MWIAR 171u .999829 0 nMWI1 nMWIi1    # secondary surface (AR coated)
m MWIAR 0 0 0 nMWI1 nMWIi1            # West arm "misaligned"
s sMWI .097 $nsilica nMWIi1 nMWIi2
m MWI 0.883 0.1166 0 nMWIi2 nMWI2     # nominal values 
#m MWI 0.881 0.1166 0 nMWIi2 nMWI2      # extra losses of 0.2% 

s sWl 2999.9 nMWI2 nMWE1               # cavity length

# West End Mirror T=50ppm, Loss=50ppm
m MWE 0.9999 38.3u 0.0 nMWE1 nMWEi1   
s sMWE .096 $nsilica nMWEi1 nMWEi2     # fused silica substrate
m MWEAR 0 1 0 nMWEi2 nMWE2             # secondary surface (AR coated)
attr MWE Rc 3620                      # nominal radius of curvature of WE
#attr MWE Rc 3530                      # radius of curvature matched to NE
#attr MWE Rc 3600                       # radius of curvature with some mismatch to NE



#################### DETECTORS TABLE  DIODES ###############
# dummy length with proper Gouy settings to be used
# instead of full telescope
s sDT1 0.937 nM66 nDTsplit1
attr sDT1 g 0
bs bsDT .25 .25 0 0 nDTsplit1 nDTsplit2 nDTsplit3 dump
s sQ21 0.4 nDTsplit2 nQ21
attr sQ21 g 20
s sQ22 0.76 nDTsplit3 nQ22
attr sQ22 g 110

/*
pd2 Q211 $fMI 0 $fsig nQ21 
pdtype Q211 y-split

pd2 Q212 $fMI 90 $fsig 0 nQ21
pdtype Q212 y-split

pd2 Q221 $fMI 0 $fsig 0 nQ22
pdtype Q221 y-split

pd2 Q222 $fMI 90 $fsig 0 nQ22
pdtype Q222 y-split
*/

######################## DETECTION DIODES ##################
# dummy length with proper Gouy settings to be used
# instead of full telescope
/*
s sDB1 14.738 nBSAR nDBsplit1
attr sDB1 g 0
bs bsDB .25 .25 0 0 nDBsplit1 nDBsplit2 nDBsplit3 dump
s sQ51 0.9 nDBsplit2 nQ51
attr sQ51 g 50
s sQ52 0.708 nDBsplit3 nQ52
attr sQ52 g 140



pd2 Q511 $fMI 0 $fsig nQ51 
pdtype Q511 y-split

pd2 Q512 $fMI 22 $fsig 0 nQ51
pdtype Q512 y-split

pd2 Q521 $fMI -5 $fsig 0 nQ52
pdtype Q521 y-split
pd2 Q522 $fMI 85 $fsig 0 nQ52
pdtype Q522 y-split
*/

############### NORTH BENCH DIODES #####################
# dummy length with proper Gouy settings to be used
# instead of full telescope
s sNB1 3.5542 nMNE2 nNBsplit1
attr sNB1 g 0
bs bsNB .25 .25 0 0 nNBsplit1 nNBsplit2 nNBsplit3 dump
s sQ71 0.859 nNBsplit2 nQ71
attr sQ71 g 40
s sQ72 1.118 nNBsplit3 nQ72
attr sQ72 g 130

/*
pd2 Q711 $fMI 0 $fsig nQ71 
pdtype Q711 y-split

pd2 Q712 $fMI 22 $fsig 0 nQ71
pdtype Q712 y-split

pd2 Q721 $fMI -5 $fsig 0 nQ72
pdtype Q721 y-split
pd2 Q722 $fMI 85 $fsig 0 nQ72
pdtype Q722 y-split
*/

######################## WEST BENCH DIODES #################
# dummy length with proper Gouy settings to be used
# instead of full telescope
s sWB1 3.5542 nMWE2 nWBsplit1
attr sWB1 g 0
bs bsWB .25 .25 0 0 nWBsplit1 nWBsplit2 nWBsplit3 dump
s sQ81 0.859 nWBsplit2 nQ81
attr sQ81 g 40
s sQ82 1.118 nWBsplit3 nQ82
attr sQ82 g 130

/*
pd2 Q811 $fMI 0 $fsig nQ81
pdtype Q811 y-split

pd2 Q812 $fMI 22 $fsig 0 nQ81
pdtype Q812 y-split

pd2 Q821 $fMI -5 $fsig 0 nQ82
pdtype Q821 y-split
pd2 Q822 $fMI 85 $fsig 0 nQ82
pdtype Q822 y-split
*/

######################## COMMANDS ##########################
const fsig .001
cav NC MNI nMNI2 MNE nMNE1       # compute north cavity
cav WC MWI nMWI2 MWE nMWE1       # compute west cavity

maxtem 1                       # order of higher order modes (n+m)
yaxis lin abs
#pause
gnuterm no
#trace 8

########################## DC POWERS ######################


#pd B1 nBSe
#pd B7 nMNE1
#pd B8 nMWE1
#pd BSp nBSs
#pd B2 nMPR1

pd2 B1p $fMI 0 $fsig 0 nBSe 
#pd2 B1q $fMI 90 $fsig 0 nBSe 

fsig sig1 MNE $fsig 180

xaxis B1p phase1 lin 0 50 200


/*
pd1 B7acp $fMI 0 nMNE1
pd1 B7acq $fMI 90 nMNE1
pd1 B8acp $fMI 0 nMWE1
pd1 B8acq $fMI 90 nMWE1

#xaxis i1 f lin 501.115M 501.125M 2000

xaxis MNIAR phi lin 0 180 200

*/

/*
fsig sig1 MNI y $fsig 0
#xaxis Q711 phi1 lin 0 180 200
xaxis sig1 f log 1 1k 200
#xparam Q211 f2 1 0
#xparam Q511 f2 1 0
#xparam Q711 f2 1 0
xparam Q811 f2 1 0


#xaxis BS phi lin -100 100 200
#xaxis MNE phi lin -100 100 200
#xparam MNI phi -1 0
#xparam MWE phi 1 0
#xaxis MPR phi lin -10 10 200
*/

gnuterm no

#gnuplot parameters for beam imaging
GNUPLOT
set nosurface
unset hidden3d
set colorbox vertical
set colorbox user origin .85,.1 size .04,.8
set style line 100 lt -1 lw 0
set pm3d map
set title 0,3
set format z '%.1g'
set format cb '%.1g'
#set size ratio 1
#unset grid
#unset label
END
