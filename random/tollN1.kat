#-----------------------------------------------------------------------
# adf 311002
#
# Signals for SR lock (acquisition)
#
#  previously determined 
#  optimum Schnupp freq. 14.904974 MHz (upper SB)
#                          14.904950 MHz (lower SB)
#    -> 14.904962 MHz
#


l i1 1 0 nlaser
s sin 0 nlaser nMU3_1
#gauss beam_in i1 nLaser 1m -5.4 

mod  eom5 37.16M .3 1 pm 0 nMU3_1 nMU3_2_1  	# PRC control
s s0 0 nMU3_2_1 nMU3_2_2
#mod  eom4 12.02448M .3 2 pm 0 nMU3in nMU3_1  	# Schnupp1 (SR control)
#mod  eom5 14.905093M .5 1 pm 0 nMU3_1 nMU3_2  	# Schnupp2 (MI control)
#lens lpr 1.8 nMU3_2 nMU3_3
# some rather arbitrary thermal lense for the isolators and the EOMs:
#lens therm 5.2 nMU3_3 nMU3_4
isol d2 120 nMU3_2_2 nMU3out               	# Faraday Isolator


# dummy beamsplitter to apply freq. modulation peak
s    sx1 1m nMU3out nbsm1
bs bsmod 1 0 0 0 nbsm1 nbsm2 dump dump


# 070502 corrected length with respect to OptoCad (Roland Schilling)
s    smcpr3 4.391 nbsm2 nBDIPR1  
bs1  BDIPR 50e-6 30e-6 0 45 nBDIPR1 nBDIPR2 dump dump
s    smcpr4 0.11 nBDIPR2 nPRo


##------------------------------------------------------------ 
## main interferometer ##
##
## Mirror specifications for the _final_ optics are used.
##

# first (curved) surface of MPR
#m    mPRo 0 1 0 nMPRo nMPRi
#attr mPRo Rc -1.867     # Rc as specified 
#attr mPRo Rc -1.85842517051051 # Rc as used in OptoCad (layout_1.41.ocd)
#s    smpr 0.075 1.44963 nMPRi nPRo
# second (inner) surface of MPR
#m1   MPR 1000e-6 38e-6 0. nPRo nPRi   	# 1000ppm power recycling
#m   MPR 0 1 0. nPRo nPRi       	# no power recycling
m   MPR 0.9865 0.0135 90. nPRo nPRia_1       	# T=1.35% PR
s s1 0 nPRia_1 nPRia_2
bs   Mloss 0. 1 0. 0 nPRia_2 dump nPRi dump

s    swest 1.145 nPRi nBSwest

## BS
##
##                              
##                       nBSnorth     ,'-.
##                             |     +    `. 
##                             |   ,'       :'
##            nBSwest          |  +i1      +
##         ---------------->    ,:._  i2 ,'
##                             + \  `-. +       nBSeast
##                           ,' i3\   ,' ---------------
##                          +      \ +
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nBSsouth
##                             -   |
##                                 |
##                                 |


bs   BS 0.4859975 0.5139975 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#s    sBS1a 0.041 1.44963 nBSi1 nBSi1b
# here thermal lense of beam splitter (Roland: f about 1000m for 10kW at BS)
#lens bst 1k nBSi1a nBSi1b
s    sBS1 0.091 1.44963 nBSi1 nBSi2
s    sBS2 0.091 1.44963 nBSi3 nBSi4
bs   BS2 1000u 0.999 0 -27.9694 nBSi2 dump nBSeast nSRdet		# 40ppm AR coating
bs   BS3 1000u 0.999 0 -27.9694 nBSi4 dump nBSsouth dump	# 40ppm AR coating

## north arm
s snorth1 598.5682 nBSnorth nMFN1
bs1 MFN 50e-6 10e-6 0.0 0.0 nMFN1 nMFN2 dump dump
#attr MFN Rc 640
s snorth2 597.0108 nMFN2 nMCN1
m1 MCN 50e-6 10e-6 0.0 nMCN1 dump
#m MCN 0 1 0.0 nMCN1 dump
#attr MCN Rc 601

## east arm
s seast1 598.4497 nBSeast nMFE1
bs1 MFE 50e-6 10e-6 0.0 0.0 nMFE1 nMFE2 dump dump
#attr MFE Rc 638

s seast2 597.0663 nMFE2 nMCE1
m1 MCE 50e-6 10e-6 0.0 nMCE1 dump  
#attr MCE Rc 713
#attr MCE ybeta .1u

## south arm
s ssouth 1.103 nBSsouth nMSRi  # 9cm cav length diff

#m MSR 0.99 0.01 0.0 nMSRi nMSRo         	# broadband
#m MSR 0.993 0.006971 0.2 nMSRin nMSRo 	        # about 150 Hz detuning
#m MSR 0.98 0.006971 0.7 nMSRin nMSRo   	# about 500 Hz detuning
m MSR .0 1. 0.0 nMSRi nMSRo              	# No Signal recycling

##########################################################################################

#fsig f1 bsmod 474 0


#pd0 pw nBSwest
#pd0 ps nBSeast


fsig f1 MPR 1 0 
#fsig f2 MCE 1 180

#xaxis* MSR phi lin -90 90 100
xaxis* MPR phi lin -90 90 200

x2axis* MCN phi lin 0 180 200
func mx = (-1) * $x2
put MCE phi $mx

#x2param MCE phi -1 0

pd2 outp  37.16M -20 1 max nPRo
#pd1 outp  14.905093M 50 nSRdet
#pd2 outp  14.905093M 111 1 max nMSRo
#pd1 outp  14.905093M 111 nMSRo
#pd1 outq 14.905093M 21   nMSRo

#xaxis outp phi1 lin -200 200 200
#xaxis Mloss T lin 0.9 1 400
#xaxis* eom5 f lin -200 200 200
#xparam outp f1 1 0
#xparam outq f1 1 0


yaxis log abs

gnuterm no                   # gnuplot terminal: x11


##------------------------------------------------------------ 
## commands
#maxtem 2
time
#trace 7
#phase 3
# MC1 cavity
#cav mc1 MMC1a nMC1_0 MMC1a nMC1_5
# MC2 cavity
#cav mc2 MMC2a nMC2_0 MMC2a nMC2_5
# PR cavity (north arm)
#cav prc1 MPR nPRi MCN nMCN1 # PR cavity (east arm)
#cav prc2 MPR nPRi MCE nMCE1 
# SR cavity (north arm)
#cav src1 MSR nMSRi MCN nMCN1 
# SR cavity (east arm)
#cav src1 MSR nMSRi MCE nMCE1 

##------------------------------------------------------------ 
## Outputs

GNUPLOT
#set view 60, 140, ,
#set hidden3d
#set zrange[1e-5:100]
set cbrange[1e-5:100]
#set cntrparam levels 20
#set contour
#set view 0, 0, ,
set nosurface
unset hidden3d
set colorbox v
set colorbox user origin .95,.1 size .04,.8
#set pal noborder
#set style line 100 lt 19 lw 0 #gif1
set xtics 45
set style line 100 lt -1 lw 0
#set pm3d at s hidden3d 100 solid
set log cb
set pm3d map
#set pm3d at s

#-----------------------Colors--------------------------------
# -- traditional pm3d (black-blue-red-yellow)
#set palette rgbformulae 7,5,15
# -- green-red-violet
#set palette rgbformulae 3,11,6
# -- ocean (green-blue-white)  try also all other permutations
#set palette rgbformulae 23,28,3
# -- hot  (black-red-yellow-white)
#set palette rgbformulae 21,22,23
# --  colour printable on gray (black-blue-violet-yellow-white)
#set palette rgbformulae 30,31,32
# -- rainbow (blue-green-yellow-red)
#set palette rgbformulae 33,13,10
# -- AFM hot (black-red-yellow-white)
set palette rgbformulae 34,35,36
#-------------------------------------------------------------

#set format z '%.1g'
#set format y '%.1g'

#set xlabel  0,1
#set title "blabla"
set label "MI phi [deg]" at screen .05,.45 rotate
#set nokey
#set title "pow_trans"
#unset grid
END


