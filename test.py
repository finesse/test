#!/bin/python
from __future__ import print_function
import sys
import subprocess as sub
import numpy as np
import os
import os.path as path
import requests
import traceback
from configparser import ConfigParser

# ===========
# Exceptions
# ===========

# Setting the exception hooks
# ---------------------------

class ExitCodeException(Exception):
    """ Base class for all exceptions which set the exit code"""
    def getExitCode(self):
        "meant to be overridden in subclass"
        return 1

def handleUncaughtException(exctype, value, trace):
    """ Function to exit with error code from exception object """
    oldHook(exctype, value, trace)
    if isinstance(value, ExitCodeException):
        sys.exit(value.getExitCode())

# Set the exception hook to be our new function which exits with a error code
# Store old hook as a variable
sys.excepthook, oldHook = handleUncaughtException, sys.excepthook

# Defining new exceptions
# --------------------------

class RunException(ExitCodeException):
    """ Exception for failures of finesse to run """
    def __init__(self, returncode, args, err, out):
        self.returncode = returncode
        self.args = args
        self.err = err
        self.out = out
        super(RunException, self).__init__(returncode, args, err, out)

    def getExitCode(self):
        return 3

class DiffException(ExitCodeException):
    """ Exception for differences against the reference file """
    def __init__(self, msg, outfile):
        self.msg = msg
        self.outfile = outfile

    def getExitCode(self):
        return 5

# ===========
# Functions
# ===========
def runkat(katfile):
    """ Run the katfile """
    try:
        p = sub.Popen(["kat","--test",katfile], stdout=sub.PIPE, stderr=sub.PIPE)
        out, err = p.communicate()
    finally:
        if 'p' in locals():
            try:
                p.kill()
            except OSError:
                pass

    # BrumSoftTest once we leave this file, stdout and stderr get mapped though
    # docker back to the system stdout and stderr on the host server
    # This can only handle ascii, so here we re-encode the data stream as ascii
    encoding=sys.stdout.encoding
    if encoding==None:
        encoding='utf-8'

    def fixencoding(var):
        if isinstance(var,bytes): var = var.decode(encoding)
        else: var = str(var)

        return var.encode('ascii','replace').decode('ascii')

    out,err = fixencoding(out),fixencoding(err)
    
    print(out)
    print(err, file=sys.stderr)

    # if finesse exited with a non-zero error code we raise a run exception (which maps to error code 3)
    if p.returncode != 0:
        raise RunException(p.returncode, "kat "+katfile, err, out)

    return [out,err,p.returncode]


def diff(katfile,finesse3=False):
    """ If everything ran okay we will calculate the differance """
    diffFound = False

    katdir=path.dirname(katfile)
    katname=str(path.basename(katfile)).split('.')[0]


    # Check files exists
    REF_DIR=path.join(katdir,'reference')
     
    if not path.exists(REF_DIR):
        raise Exception("Suite reference directory doesn't exist: " + REF_DIR)

    ref_file = path.join(REF_DIR,katname+'.out')
    out_file = path.join(katdir,katname+'.out')
    tol_file = path.join(REF_DIR,katname+'.tol')
    diff_file = path.join(katdir,katname+'.diff')
    diff_plt = path.join(katdir,katname+'.pdf')

    if not path.exists(ref_file):
        raise Exception("Reference file doesn't exist for " + ref_file, ref_file)

    if not path.exists(out_file):
        raise Exception("Cannot locate output file for " + out_file, out_file)
    
    if finesse3:
        tol_file = tol_file+'3'

    ## Load data
    ref_arr = np.genfromtxt(ref_file, dtype=np.float64, comments='%')
    out_arr = np.genfromtxt(out_file, dtype=np.float64, comments='%')

    # Check same shape
    if ref_arr.shape != out_arr.shape:
        raise DiffException("Reference and output are different shapes: ref " + str(ref_arr.shape)
                + ", out " + str(out_arr.shape), out_file)

    # The philisophy here is that atol is just to catch 
    # zero crossings and so tests pass when atol or rtol pass
    #
    # Tol is an ini file containing tolerance infomation
    # It may contains any of the sections
    #
    # This script test that it meets at least one of 
    # rtol ***OR**** atol. Not both.
    # Set atol to zero to test only atol.
    # 0 values are automatically masked out
    # from rtol computations
    # 
    #  ```
    # [DEFAULT]
    # rel_err = float_like #default relative error for this file
    # abs_err = float_like #default abs error for this file
    #
    #
    # [int_like] # override for a specific coloum, identified 0 based int
    # rel_err = float_like
    # abs_err = float_like
    # ```
    #
    # Example
    # ```    #
    # [0] # override for x axis
    # rel_err = None # dont use rel_err in xaxis
    # abs_err = 1e-14
    #
    # [1] # is a phase measurement so allow an abs_err
    # rel_err = 1e-12
    # abs_err = 1e-15
    tol_config = ConfigParser()

    if finesse3:
        tol_config.read_string("""
[DEFAULT]
rel_tol = 1e-10
abs_tol = 0
""")

    else:
        tol_config.read_string("""
[DEFAULT]
rel_tol = 1e-12
abs_tol = 0
""")

    if path.exists(tol_file):
        print("Tolerance file found, setting tolerance")
        tol_config.read(tol_file)

    # Create empty arrays to hold tols
    abs_tol = np.empty(ref_arr.shape)
    rel_tol = np.empty(ref_arr.shape)

    # Populate with defaults
    _tol = tol_config.getfloat('DEFAULT','abs_tol',fallback=0)
    print('Setting default abs_tol to: {}'.format(_tol))
    abs_tol[:] = _tol

    _tol = tol_config.getfloat('DEFAULT','rel_tol',fallback=1e-12)
    print('Setting default rel_tol to: {}'.format(_tol))
    rel_tol[:] = _tol

    # Read coloumns and change tol values
    for column in tol_config.sections():
        try:
            idxc = int(column)
        except ValueError:
            print('Could not interpret {} as a column. Please use a integer.'.format(column))
            next
        
        _tol = tol_config.getfloat(column,'abs_tol',fallback=0)
        print('Setting column {} abs_tol to: {}'.format(column,_tol))
        abs_tol[:,idxc] = _tol

        _tol = tol_config.getfloat(column,'rel_tol',fallback=0)
        print('Setting column {} rel_tol to: {}'.format(column,_tol))
        rel_tol[:,idxc] = _tol

    # ----------------------------
    #  Absolute Error Computation
    # ----------------------------
    abs_diff = np.abs(out_arr-ref_arr)
    rel_diff = np.empty(abs_diff.shape)
    

    # ------------------------------
    # Relative Error Computation
    # ------------------------------
    # We can only define a relative error
    # for non zero values
    # ------------------------------

    # Find all the zero indicies
    zix = np.logical_or(ref_arr == 0,out_arr==0) # zero index

    # Set to inf, so rtol will be False
    # then these will only pass when abs_err passes
    rel_diff[zix] = np.inf 
    
    # Find the both zero indicies
    bzix = np.logical_and(ref_arr == 0,out_arr==0) # both
    
    # When both the ref_arr and out_arr
    # match, we set r_err = 0
    # So that we dont need to set an atol
    # if everything matches perfectly
    rel_diff[bzix] = 0 

    nzix = ~zix # non-zero index
    del zix, bzix # free up some memory for plotting
    
    # Now for all the non-zero indcies compute a relative error
    rel_diff[nzix] = np.divide(abs_diff[nzix], np.abs(ref_arr[nzix]))

    # save data 
    np.savetxt(diff_file,rel_diff)

    def is1d():
        return len(ref_arr.shape) == 1

    def is2d(pltinfo):
        if isinstance(pltinfo,str):
            if '2D plot' in pltinfo:
                return True
        return False

    def is3d(pltinfo):
        if isinstance(pltinfo,str):
            if '3D plot' in pltinfo:
                return True
        return False

    # Get header infomation
    with open(out_file,'r') as f:
        finesse_ver = f.readline().replace('%','')
        pltinfo = f.readline()
        hdr = f.readline()

        if is2d(pltinfo) or is3d(pltinfo):
            hdr = hdr.replace('%','').replace('\n','').split(',')
            hdr = [x.strip() for x in hdr]
        else:
            # Probably all junk
            finesse_ver = pltinfo = hdr = None

    if is2d(pltinfo) and not is1d():
        try:
            from matplotlib import pyplot as plt
        except ImportError:
            print('Cannot plot, matplotlib is not installed')
        else:
            print('Attempting to plot error behavior')
            
            try:
                Nlines = 7 # number of lines per plot
                nplt = int(np.ceil(len(hdr) / Nlines))

                fig, axes = plt.subplots(nrows=nplt,
                                        figsize=(10,8*nplt))

                if nplt == 1:
                    axes = [axes]
                    
                for i in range(0,rel_diff.shape[1]):
                    pid = int(np.floor(i / Nlines))
                    axes[pid].semilogy(out_arr[:,0],np.abs(rel_diff[:,i]),label=hdr[i])

                # If everything positive (if anything negative then linspace)
                if np.all([out_arr[:,0] > 0.0]):
                    # Determine if the data is logspaces
                    # If the points are equally spaced then we know it is
                    diff_first2 = out_arr[1,0] - out_arr[0,0]
                    diff_last2 = out_arr[-2,0] - out_arr[-1,0]
                    rel_xaxis_change = np.abs((diff_first2 - diff_last2)/diff_first2)

                    if rel_xaxis_change > 1.2 or rel_xaxis_change < 0.8:
                        axes[0].set_xscale('log')

                axes[-1].set_xlabel(hdr[0])

                for ax in axes:
                    ax.set_ylabel('Relative Difference [arb]')
                    ax.legend(loc=(1.05,0.1))
                    ax.grid(True)
                
                fig.savefig(diff_plt,bbox_inches='tight')
                print('Error behaviour saved to: '+str(diff_plt))
            except Exception:
                import traceback
                traceback.print_exc()
    else:
        print('Could not find header infomation or not 2D, or noxaxis was set. Cannot plot.')


    # check for nans
    if np.any(np.isnan(rel_diff)):
        raise DiffException("nan found in outut!", katfile)

    # This block of code works subtly differently to np.isclose
    # instead it requires that the error is smaller than
    # atol OR rtol, unless it is disabled
    passed = np.logical_or(
                np.abs(abs_diff) <= abs_tol,
                np.abs(rel_diff) <= rel_tol
                )
    failed = ~passed
    # I think np.allclose could probably be used here instead
    # but I think it is less transparent
    #import pdb; pdb.set_trace()
    
    if np.any(failed):
        
        print('{} data points where out of tolerance!'.format(np.count_nonzero(failed)))
        
        # Print the row with the largest relative diff
        max_rdiff = np.max(rel_diff[failed])
        ix = np.where(max_rdiff == rel_diff)
        print('The row with the largest relative error is:')        
        if len(ix[0]) > 1:
            print('Note {} rows have an identical error, selecting first.'.format(len(ix[0]))) 
            ix = ix[0][0],ix[1][0]
        elif len(ix) == 0: 
            print('Could not locate ix, something is wrong')
            raise Exception

        _stack = np.vstack((ref_arr[ix[0],:], out_arr[ix[0],:],
                            rel_diff[ix[0],:],rel_tol[ix[0],:],
                            abs_diff[ix[0],:],abs_tol[ix[0],:])).T

        if hdr is not None:
            # Determine widths of coloums and format
            maxchar_hdr = max([len(x) for x in hdr])
            row_format = "{:<{lnhdr}}"+"{:>+28.16e}"*6
            
            print(('  {:<{lnhdr}}'+'{:>28}'*6).format('Name',
             'Reference Array','Output Array', 
             'Relative Differance', 'Relative Tolerance',
             'Absolute Differance', 'Absolute Tolerance',lnhdr=maxchar_hdr+3))
            
            for i in range(_stack.shape[0]):
                pre='  '
                if i == ix[1]: pre = '**'
                print(pre+row_format.format(hdr[i], *tuple(_stack[i,:]),
                 lnhdr=maxchar_hdr+3))

        else:
            hdr = None
            print('Cannot print advanced debugging info as header not present')
            print('Reference Array\t\tOutput Array\t\t'+
                  'Relative Diff\t\tRelative Tolerance\t\t'+
                  'Absolute Diff\t\tAbsolute Tolerance')
            print(repr(_stack))

        print('Offending line: '+repr(_stack[ix[1],0:3]))
        print('Max relative difference: '+str(rel_diff[ix]))
        print('Corresponding absolute difference: '+str(abs_diff[ix]))

        raise DiffException("Out of tolerance!", katfile)
            
    else:
        print('Matched to within tolerance!')
        print('Max relative difference: '+str(np.max(rel_diff)))
        print('Max absolute difference: '+str(np.max(abs_diff)))

    return

def do_upload(test_kat):
    katdir=path.dirname(test_kat)
    katname=str(path.basename(test_kat)).split('.')[0]
    
    out_file = path.join(katdir,katname+'.out')
    ref_file = path.join(katdir,'reference',katname)
    diff_file = path.join(katdir,katname+'.diff')
    diff_plt = path.join(katdir,katname+'.pdf')

    os.rename(str(ref_file)+'.out',str(ref_file)+'.ref')
    ref_file = str(ref_file)+'.ref'

    print('Uploading: '+str(out_file)+" To: "+str(sys.argv[2]))
    with open(out_file, 'rb') as f: r = requests.post(sys.argv[2], files={'file': f},verify=False)
    print("Server Returns: " + str(r.text))
    r.raise_for_status()

    print('Uploading: '+str(ref_file)+" To: "+str(sys.argv[2]))
    with open(ref_file, 'rb') as f: r = requests.post(sys.argv[2], files={'file': f},verify=False)
    print("Server Returns: " + str(r.text))
    r.raise_for_status() 

    print('Uploading: '+str(diff_file)+" To: "+str(sys.argv[2]))
    with open(diff_file, 'rb') as f: r = requests.post(sys.argv[2], files={'file': f},verify=False)
    print("Server Returns: " + str(r.text))
    r.raise_for_status() 

    if path.exists(diff_plt):
        print('Uploading: '+str(diff_plt)+" To: "+str(sys.argv[2]))
        with open(diff_plt, 'rb') as f: r = requests.post(sys.argv[2], files={'file': f},verify=False)
        print("Server Returns: " + str(r.text))
        r.raise_for_status() 
    else:
        print('no plot file found to upload')

if __name__ == "__main__":
    print("Kat Test Started, Arguments: "+str(sys.argv))
    test_kat = sys.argv[1]
    
    try: 
        runkat(test_kat)
    except Exception as e:
        errtext = 'Error running finesse. Output file not produced, reference file not used. These will not be uploaded'
        print(errtext)
        print(errtext, file=sys.stderr)
        raise e

    else:
        print('Finesse ran without error')

    try:
        diff(test_kat)
    finally:
        if len(sys.argv) > 2:
            try:
                do_upload(test_kat)
            except Exception:
                print(traceback.format_exc())
