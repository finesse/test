% read from file "geo_sens1.kat"
%%% FTblock common
##------------------------------------------------------------ 
## MU 3 
##
## for simulations of the MI only, use the following block.
## for full simulation, including the MCs, comment out the following
## block and uncomment the code above
#l i1 2.2 0 nMU3in1                            # reduced light power 
l i1 3.2 0 nMU3in1                            # nominal S5 corrsponds to 75deg
gauss beam_in i1 nMU3in1 268u -550m           # beam parameter roughly matching PRC
# (old value:  i1 nMU3in 248u -550m)       ** to be checked **
mod  eom3 $fPR $midxPR 3 pm 0 nMU3in1 nMU3in_1  # PRC control
s s_0 0 nMU3in_1 nMU3in_2
##--------------------------------------------------------------
mod  eom4 $fSR $midxSR 3 pm 0 nMU3in_2 nMU3_2_1      # Schnupp1 (SR control)
s s_0 0 nMU3_2_1 nMU3_2_2
mod  eom5 $fMI $midxMI 3 pm 0 nMU3_2_2 nMU3_3_1      # Schnupp2 (MI control)
s s_1 0 nMU3_3_1 nMU3_3_2
lens lpr 1.8 nMU3_3_2 nMU3_4_1
s s_2 0 nMU3_4_1 nMU3_4_2
# some rather arbitrary thermal lense for the isolators and the EOMs:
lens therm 5.2 nMU3_4_2 nMU3_5_1                     # ** to be checked **
s s_3 0 nMU3_5_1 nMU3_5_2
isol d2 120 nMU3_5_2 nMU3out               	       # Faraday Isolator
# 070502 corrected length with respect to OptoCad (Roland Schilling)
s    smcpr3 4.391 nMU3out nBDIPR1  
bs1  BDIPR 50u 30u 0 45 nBDIPR1 nBDIPR2 dump dump
s    smcpr4 0.11 nBDIPR2 nMPR1
##------------------------------------------------------------ 
## main interferometer ##
##
## New MPR; values for MPR005 page 2264 (check with Harald)
## first (curved) surface of MPR
m    mPRo 0 1 0 nMPR1 nMPRi1
attr mPRo Rc -1.85
s    smpr 0.0718 1.44963 nMPRi1 nMPRi2
# second (inner) surface of MPR
m1   MPR $TMPR $LMPR $MPRphi nMPRi2 nMPR2       	     # T=900 ppm, L=50 ppm
s    swest 1.1463 nMPR2 nBSwest		# new length with T_PR=900 ppm ** to be checked **
##------------------------------------------------------------ 
## BS 
## basic data from old GEO files
##
##                              
##                       nBSnorth     ,'-.
##                             |     +    `. 
##                             |   ,'       :'
##            nBSwest          |  +i1      +
##         ---------------->    ,:._  i2 ,'
##                             + \  `-. +       nBSeast
##                           ,' i3\   ,' ---------------
##                          +      \ +
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nBSsouth
##                             -   |
##                                 |
##                                 |
bs2   BS $RBS $LBS $BSphi 42.834 nBSwest nBSnorth nBSi1 nBSi3 
s    sBS1a 0.040 1.44963 nBSi1 nBSi1b
##------------------------------------------------------------ 
# Thermal lense of beam splitter 
lens bst 9k nBSi1b nBSi1c # static value for 1.9kW at BS
## -------------------------------------------------------------
# Alternative: dynamic thermal lens computation, as in: 
# S. Hild et al, Applied Optics IP, vol. 45, Issue 28, pp.7269-7272
# assuming 0.25ppm/cm absorption, w=0.88cm, d=9cm
s    sBS1 0.051 1.44963 nBSi1c nBSi2
s    sBS2 0.091 1.44963 nBSi3 nBSi4
bs2   BS2 $RBSAR $LBSAR 0  27.9694 nBSi2 dump nBSeast nBSAR  # R=60 ppm, L=30ppm
bs2   BS3 $RBSAR $LBSAR 0 -27.9694 nBSi4 dump nBSsouth dump	 # R=60 ppm, L=30ppm
# two measured values for R_AR: labbook page 2418 (44ppm), 3996 (64ppm)
##------------------------------------------------------------ 
## north arm
s snorth1 598.5682 nBSnorth nMFN1  # ** to be checked **
bs1 MFN $TMFN $LMFN $MFNphi 0.0 nMFN1 nMFN2 dump dump  # T=8.3 ppm
attr MFN Rc 666
s snorth2 597.0241 nMFN2 nMCN1 # ** to be checked **
m1 MCN $TMCN $LMCN $MCNphi nMCN1 dump                  # T=13 ppm
attr MCN Rc 636
##------------------------------------------------------------ 
## east arm 
s seast1 598.4497 nBSeast nMFE1
bs1 MFE $TMFE $LMFE $MFEphi 0.0 nMFE1 nMFE2 dump dump  # T=8.3 ppm 
# ** the Rc(T) below need to be improved **
attr MFE Rcx 667	# 90 W
attr MFE Rcy 667	# 90 W
#attr MFE Rcx 687	# 0 W
#attr MFE Rcy 687	# 0 W
s seast2 597.0630 nMFE2 nMCE1
m1 MCE $TMCE $LMCE $MCEphi nMCE1 dump                   # T=13ppm
attr MCE Rc 622
##------------------------------------------------------------ 
## south arm 
s ssouth 1.109 nBSsouth nMSR1
m MSR $RMSR $TMSR $MSRphi nMSR1 nMSR2 	# R=0.9805, T=0.01945, L=50 ppm
##------------------------------------------------------------ 
## further settings and commands
# Modulation frequencies and indicees
const fMC1 25M
const fMC2 13M
const fSR 9016865    ## corresponding to 532 Hz on tune.vi, (10/2006 S. Hild)
const fMI 14.904929M ## (10/2006 S. Hild)
const fPR  37.16M	
const midxPR 0.13 # see page 4011
const midxSR 0.17 # see page 4011
const midxMI 0.38 # see page 4011
# Michelson reflectivities/tranmsissions
const TMPR  900u 
const RBS   0.485998
const RBSAR 60u
const TMCN  13u
const TMFN  8.3u
const TMCE  13u
const TMFE  8.3u
const RMSR  0.9805
const TMSR  0.01945
#const RMSR  0
#const TMSR  1
# Michelson losses, see labbook page 4027
const LMPR  130u 
const LMCN  130u
const LMFN  130u
const LMCE  130u
const LMFE  130u
const LBS   130u
const LBSAR 130u
# Dual-recycled Michelson operating point
const MPRphi 0.0
const BSphi  0.0
const MFNphi 0.0
const MFEphi 0.0
const MCNphi 0.0
const MCEphi 0.0
#const MSRphi 0.7646
const MSRphi 0.71
# MSR tuning = f_tune/FSR_SR * 180, FSR_SR=125241 Hz ** to be checked **
# e.g f_tune 532 Hz -> 0.7646 deg
# SR cavity (north arm)
cav src1 MSR nMSR1 MCN nMCN1 
# SR cavity (east arm)
cav src2 MSR nMSR1 MCE nMCE1 
# PR cavity (north arm)
cav prc1 MPR nMPR2 MCN nMCN1 
# PR cavity (east arm)
cav prc2 MPR nMPR2 MCE nMCE1 
const pi 3.141592653589793
##----------------------------------------------------------------
#
# Shotnoise limited sensitivity:
#
# The following shows and compares 4 different ways of computing
# a shotnoise-limited sensitivity of GEO.
#
%%% FTend common

% read from file "geo_sens1.kat"
%%% FTblock plot
yaxis log abs
maxtem 3
retrace off
time
phase 3
pause
gnuterm no
GNUPLOT
set yrange[1e-22:1e-20]
set ytics (\
"10^{-22}" 1e-22,\
"" 2e-22,\
"" 3e-22,\
"" 4e-22,\
"" 5e-22,\
"" 6e-22,\
"" 7e-22,\
"" 8e-22,\
"" 9e-22,\
"10^{-21}" 1e-21,\
"" 2e-21,\
"" 3e-21,\
"" 4e-21,\
"" 5e-21,\
"" 6e-21,\
"" 7e-21,\
"" 8e-21,\
"" 9e-21,\
"10^{-20}" 1e-20)
END
%%% FTend plot

% read from file "geo_sens1.kat"
%%% FTblock lock5
##----------------------------------------------------------------
# 5. Using displacement and qshotS
#
# The same as above a bit more compact:
fsig sig1 MCN 1 0
fsig sig2 MCE 1 180
qshotS pdMI 2 $fMI max 1 max nMSR2
set s1 pdMI abs
# compute sens in m/sqrt(Hz)
func s2 = 1064.0E-9 / 2 /$pi * $s1 * sqrt( 6.6262E-34 * 299792458.0 / 1064E-9 ) 
noplot s2
# compute h in 1/sqrt(Hz)
func s3 = 2/1200 * $s2
noplot pdMI
xaxis sig1 f log 10 10k 300
put pdMI f2 $x1
%%% FTend lock5

