# --------------------------------------------------------------------- #
# Input file for Finesse 0.99				                                		#
#
# use with mkat !
# 
# Test of scaling factor for coupling: alignment noise into phase-noise
#	
# --------------------------------------------------------------------- #

######################## LASER ########################
# input laser, and dummy injection system
l i1 20 0 0 nin
s s0 1m nin nEO1
mod eom1 $fMI $midx 1 pm 0 nEO1 nEO2
s s2 1m nEO2 nInj
bs Inj .5 .5 0 0 nInj nMN0 dump nDT1   # simulate the faraday isolator (remember that the 
                                        # power on B2 will be wrong for a factor of 2)
######################## NORTH ARM #####################

s sNs 1m nMN0 nMNI1  

# North Input Mirror T=11.8%, 
m MNIAR $RNIAR $TNIAR $NIARphi nMNI1 nMNIi1   
s sMNI .097 $nsilica nMNIi1 nMNIi2
m MNI $RNI $TNI $NIphi nMNIi2 nMNI2        
attr MNI Rc $NIRC

s sNl $SNC nMNI2 nMNE1                  # cavity length (nominal)

# North End Mirror T=50ppm, Loss=50ppm
m MNE $RNE $TNE $NEphi nMNE1 nMNEi1    # primary surface  
s sMNE .096 $nsilica nMNEi1 nMNEi2      # fused silica substrate
m MNEAR 0 1 0 nMNEi2 nMNE2              # secondary surface (AR coated)
attr MNE Rc $NERC

cav NC MNI nMNI2 MNE nMNE1              # compute north cavity

phase 2

################# CONSTANTS #################

const SNC 3000
#const SNC 1500
const fsig 1

const fMI 1.274176875M                  # dummy mod frequency for use in reflection


const nsilica 1.44963
const midx .33                          

const NIARphi 0.0                       # Etalon of NI mirror

#const NERC 5295                         # tuned radius of curvature of NE
const NERC 3530                         # tuned radius of curvature of NE
const NIRC 0


const NIphi 0
const NEphi 0

########### Reflectivities of all mirrors ############
# recycling configuration
# 100ppm losses on the end mirrors and 150ppm on
# the input mirrors

#const RNIAR 132u
#const TNIAR 0.999868 

const RNIAR 0
const TNIAR 1

const RNI   0.8819
const TNI   0.118

const RNE   0.99985
const TNE   42.9u

###### Locking

/*
pd1 B2_ACp $fMI 0 nDT1

noplot B2_ACp

set err B2_ACp re

lock NEz $err  1 0.01                        
noplot NEz
put* MNE phi $NEz
showiterate -1
*/


/*
pd cpow nMNE1
xaxis MNI phi lin 0 30 100
*/


################## DIODE AND COMMAND

const palign 1n
const malign -1n
#const palign 0
#const malign 0

## tag run1: disabled ## attr MNI xbeta $palign
## tag run1: disabled ## xaxis MNE xbeta lin -2p 0n 10
## tag run1: disabled ## diff MNE xbeta

attr MNI xbeta $malign ## tag run2: activated
xaxis MNE xbeta lin -2p 0n 10 ## tag run2: activated
diff MNE xbeta ## tag run2: activated

## tag run3: disabled ## attr MNI xbeta $palign
## tag run3: disabled ## xaxis* MNI xbeta lin -2p 0n 10
## tag run3: disabled ## diff MNI xbeta

## tag run4: disabled ## attr MNI xbeta $malign
## tag run4: disabled ## xaxis* MNI xbeta lin -2p 0n 10
## tag run4: disabled ## diff MNI xbeta

## tag run5: disabled ## attr MNE xbeta $palign
## tag run5: disabled ## xaxis* MNE xbeta lin -2p 0n 10
## tag run5: disabled ## diff MNE xbeta

## tag run6: disabled ## attr MNE xbeta $malign
## tag run6: disabled ## xaxis* MNE xbeta lin -2p 0n 10
## tag run6: disabled ## diff MNE xbeta

## tag run7: disabled ## attr MNE xbeta $palign
## tag run7: disabled ## xaxis MNI xbeta lin -2p 0n 10
## tag run7: disabled ## diff MNI xbeta

## tag run8: disabled ## attr MNE xbeta $malign
## tag run8: disabled ## xaxis MNI xbeta lin -2p 0n 10
## tag run8: disabled ## diff MNI xbeta

pd1 B2 $fMI 0  nDT1

maxtem 1

deriv_h 1e-10

yaxis lin abs

#gnuterm x11
gnuterm no

GNUPLOT
set nosurface
#set nocolorbox
set pm3d map
#set palette gray
unset title
#set format z '%.1g'
#set size ratio 1
unset grid
END
