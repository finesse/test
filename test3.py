#!/usr/bin/env python
from __future__ import print_function
import sys
import subprocess as sub
import numpy as np
import os
import os.path as path
import requests
import traceback

import pytest


# ===========
# Exceptions
# ===========

# Setting the exception hooks
# ---------------------------
from test import ExitCodeException, handleUncaughtException

# Set the exception hook to be our new function which exits with a error code
# Store old hook as a variable
sys.excepthook, oldHook = handleUncaughtException, sys.excepthook

# Defining new exceptions
# --------------------------
from test import DiffException, ExitCodeException, RunException

# Override exception codes because we want to change them
RunException.getExitCode = lambda self: 13
RunException.__init__ = lambda self: super(RunException, self).__init__()
DiffException.getExitCode = lambda self: 15


class ParseException(ExitCodeException):
    """ Exception for differences against the reference file """

    def __init__(self):
        super(ParseException, self).__init__()

    def getExitCode(self):
        return 11


# ===========
# Functions
# ===========
from test import do_upload, diff


def runkat(katfile):
    """ Run the katfile """
    ignored_files = []

    for file in ignored_files:
        if katfile.endswith(file):
            raise Exception("Known too slow file '%s', ignoring..." % katfile)


    print("Prepping kat file")
    with open(katfile) as katf:
        katstr = katf.read()

    print("Attempting to import Finesse")
    import finesse

    print("Setting log verbosity to debug")
    finesse.configure(log_level="DEBUG")

    try:
        print("Attempting to parse katfile")
        ifo = finesse.Model()
        ifo.parse_legacy(katstr)

        # these two files have no stable cavities nor gauss commands
        # so add a gauss at laser with w0 = 2mm to mimic Finesse 2 behavior
        if katfile.endswith("temtest.kat") or katfile.endswith("ligotest2.kat"):
            ifo.parse("gauss gl l1.p1.o w0=2m z=0")
    except Exception:
        traceback.print_exc()
        raise ParseException
    else:
        print("Parsed ok!")

    try:
        print("Attempting to run katfile")
        out = ifo.run()
    except Exception:
        traceback.print_exc()
        raise RunException
    else:
        print("Ran ok!")

    try:
        print("Assembling data for comparison against Finesse2")
        
        legacy_data, column_names, plot_type = out.get_legacy_data(ifo)

        katdir = path.dirname(katfile)
        katname = str(path.basename(katfile)).split(".")[0]
        output_filename = path.join(katdir, katname + ".out")

        with open(katfile) as file:
            # Here we "cheat" a little; Finesse 3 doesn't produce an output for `func` commands,
            # and is unlikely to in the future. To solve this, we copy the data for each `func`
            # from the reference data into Finesse 3's output, assuming that `func`s always come
            # last in the reference data (which they seem to)
            import re

            num_funcs = 0
            for line in file:
                if re.search(r"^\s*func\b", line) is not None:
                    num_funcs += 1
            if num_funcs > 0:
                ref = np.genfromtxt(
                    path.join(katdir, "reference", katname + ".out"),
                    dtype=np.float64,
                    comments="%",
                )
                assert ref.shape != legacy_data.shape
                legacy_data = np.hstack(legacy_data,ref[:, -num_funcs:])
                column_names.extend(['untested']*num_funcs)
                assert ref.shape == legacy_data.shape

        out.write_legacy_data(ifo,filename=output_filename,legacy_data=legacy_data,
            column_names=column_names,plot_type=plot_type)

    except Exception:
        print("Unable to load outputs!? Has finesse.detectors.Detector changed?")
        traceback.print_exc()
        sys.exit(16)
    else:
        print('Saved Data Ok!')


def get_kats(_dir):
    import os
    files = os.listdir(_dir)
    files = [path.join(_dir,f) for f in files if f.endswith('.kat')]
    return files

@pytest.mark.parametrize("test_kat",get_kats('physics'))
def test_physics(test_kat):
    return main(test_kat)

@pytest.mark.parametrize("test_kat",get_kats('random'))
def test_random(test_kat):
    return main(test_kat)

def main(test_kat):
    try:
        runkat(test_kat)
    except Exception as e:
        errtext = "Error running finesse. Output file not produced, reference file not used. These will not be uploaded"
        print(errtext)
        print(errtext, file=sys.stderr)
        raise e

    else:
        print("Finesse3 ran without error")

    try:
        diff(test_kat, finesse3=True)
    finally:
        if len(sys.argv) > 2:
            try:
                do_upload(test_kat)
            except Exception:
                print(traceback.format_exc())


if __name__ == "__main__":
    print("Kat Test Started, Arguments: " + str(sys.argv))
    test_kat = sys.argv[1]
    main(test_kat)
